package rtb;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import json.WebCommand;
import rtb.Log.LogType;
import rtb.Log.ThreadType;
import trade.RequestThread;
import trade.StatusThread;
import trade.SwapThread;
import trade.RequestThread.Result;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class ListenServer implements Runnable {
	
	private int serverPort;
	private ServerSocket serverSocket;
	private boolean isStopped = false;
	private Thread runningThread;
	private Main main;
	private LoadBalancer loadBalancer;
	private PostgreSQLThread sqlthread;
	private StatusThread tradeStatusThread;
	private Browser browser;

	public ListenServer(Main main, LoadBalancer loadBalancer, StatusThread tradeStatusThread, Browser browser) {
		this.main = main;
		this.loadBalancer = loadBalancer;
		this.tradeStatusThread = tradeStatusThread;
		this.browser = browser;
		
		//load settings
		serverPort = Config.configuration.getInt("server_port", 8080);

		Log.println(ThreadType.LISTENER, LogType.INFO, "Initiating listen server on port: " + serverPort);
	}
	
	public void setSQLThread(PostgreSQLThread sqlthread) {
		this.sqlthread = sqlthread;
	}

	@Override
	public void run() {
		synchronized(this){
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while(!isStopped()){
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if(isStopped()) {
                	Log.println(ThreadType.LISTENER, LogType.INFO, "Server stopped");
                    return;
                }
                Log.println(ThreadType.LISTENER, LogType.DEBUG, "Error accepting client connection: " + e.getLocalizedMessage());
            }
            new Thread(new WorkerRunnable(clientSocket, main, loadBalancer, sqlthread, tradeStatusThread, browser)).start();
        }
        Log.println(ThreadType.LISTENER, LogType.INFO, "Server stopped");
	}

	private synchronized boolean isStopped() {
		return this.isStopped;
	}

	public synchronized void stop() {
		this.isStopped = true;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			Log.println(ThreadType.LISTENER, LogType.DEBUG, "Error stopping server: " + e.getLocalizedMessage());
		}
	}

	private void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
		} catch (IOException e) {
			Log.println(ThreadType.LISTENER, LogType.DEBUG, "Error opening port " + serverPort + ": " + e.getLocalizedMessage());
		}
	}
}

class WorkerRunnable implements Runnable {

	private Socket clientSocket;
	private Gson gson;
	private Main main;
	private LoadBalancer loadBalancer;
	private PostgreSQLThread sqlthread;
	private StatusThread tradeStatusThread;
	private Browser browser;

	public WorkerRunnable(Socket clientSocket, Main main, LoadBalancer loadBalancer, PostgreSQLThread sqlthread,
			StatusThread tradeStatusThread, Browser browser) {
		this.clientSocket = clientSocket;
		this.main = main;
		this.loadBalancer = loadBalancer;
		this.sqlthread = sqlthread;
		this.tradeStatusThread = tradeStatusThread;
		this.browser = browser;
		gson = new Gson();
		
	}
	
	public String sendTrade(WebCommand command, RequestThread tradeRequestThread) {
		// Get the bot to deposit or withdraw from
		// if we deposit at least 1 item, check if we are holding any of the user's items
		// and if so, deposit into the same account
		String botUsername = "";
		int spaceNeeded = 0;
		if (command.them.size() != 0) {
			botUsername = loadBalancer.getDepositBot(command.steamid);
			spaceNeeded = command.them.size();
			
			// failed to find user to deposit to
			if (botUsername == "") {
				Log.println(ThreadType.LISTENER, LogType.INFO, "CAN'T DEPOSIT ITEMS: ALL BOTS ARE FULL, MAKE MORE!!!");
				return "{\"result\": 5}";
			}
			
			boolean spacePossible = loadBalancer.canSpaceBeMade(botUsername, spaceNeeded);
			
			if (!spacePossible) {
				Log.println(ThreadType.LISTENER, LogType.INFO, "CAN'T MAKE SPACE: ALL BOTS ARE FULL, MAKE MORE!!!");
				return "{\"result\": 5}";
			}
			
			Log.println(ThreadType.LISTENER, LogType.INFO, "Depositing items on bot: " + botUsername);
		} else {
			botUsername = loadBalancer.getWithdrawBot(command.steamid);
			
			// Edge case when deposit and withdraw lists are empty and user is new
			if (botUsername == null) {
				Log.println(ThreadType.LISTENER, LogType.INFO, "Failed to find bot holding user: " + command.steamid + "'s items");
				return "{\"result\": 2}";
			}
			
			Log.println(ThreadType.LISTENER, LogType.INFO, "Withdrawing items from bot: " + botUsername);
		}
		
		// kick off thread to make a trade request
		tradeRequestThread.init(sqlthread, command, botUsername, spaceNeeded);
		tradeRequestThread.start();
		
		return "{\"result\": 1}"; //accepted
	}
	
	public void sendResponse(int statusCode, String responseString, DataOutputStream out) throws IOException {
		String statusLine;
		String serverDetails = "Server: Java HTTP server, pls no ddos\r\n";
		String contentLengthLine;
		String contentTypeLine = "Content-Type: text/html\r\n";
		
		if (statusCode == 200) {
			statusLine = "HTTP/1.1 200 OK\r\n";
		} else {
			statusLine = "HTTP/1.1 404 Not Found\r\n";
		}
		
		contentLengthLine = "Content-Length: " + responseString.length() + "\r\n";
		
		out.writeBytes(statusLine);
		out.writeBytes(serverDetails);
		out.writeBytes(contentTypeLine);
		out.writeBytes(contentLengthLine);
		out.writeBytes("Connection: close\r\n");
		out.writeBytes("\r\n");
		out.writeBytes(responseString);
	}
	
	public String handleMessage(String postString) {
		String response = "";
		
		try {
			WebCommand command = gson.fromJson(postString, WebCommand.class);
			// Spawn a new trade request thread
			RequestThread tradeRequestThread = new RequestThread(main,
					loadBalancer, browser, tradeStatusThread);
			
			// check if received message is a valid message
			if (!tradeRequestThread.validMessage(command)) {
				// tell server request was invalid
				return "{\"result\": 2}";
			}
			
			Result tradeValidity;
			switch(command.msg) {
				case 1:
					// more validity checks
					tradeValidity = tradeRequestThread.validTrade(command);
					switch (tradeValidity) {
						case VALID:
							Log.println(ThreadType.LISTENER, LogType.INFO, String.format(
									"Received valid trade request from steamid: %s. Depositing %d " + 
									"item(s), withdrawing %d item(s)", command.steamid, command.them.size(),
									command.me.size()));
							return sendTrade(command, tradeRequestThread);
						case INVALID_DEPOSIT:
							Log.println(ThreadType.LISTENER, LogType.INFO,
									"Received invalid trade request from steamid: " + command.steamid +
									". Items already on bot");
							return "{\"result\": 3}"; //items already on bot
						case INVALID_WITHDRAW:
							Log.println(ThreadType.LISTENER, LogType.INFO,
									"Received invalid trade request from steamid: " + command.steamid +
									". Items not on bot");
							return "{\"result\": 4}"; //items not on bot
						default:
							Log.println(ThreadType.LISTENER, LogType.DEBUG, "Unknown result type: " + tradeValidity);
							break;
					}
					break;
				case 2:
					// status
					// check list of active transactions
					Transaction activeTransaction = tradeStatusThread.getTransaction(command.id);
					
					// if we didn't find it under active transactions, check DB
					if (activeTransaction == null) {
						// search database for result
						Transaction tx = sqlthread.getTransaction(command.id);
						
						// status in DB is slightly different to what roshpit expects
						int responseStatus = 0;
						switch (tx.status){
							case 2: //trade is pending
								responseStatus = 5; //pending
								break;
							case 3: //trade accepted
								responseStatus = 1; //accepted
								break;
							case 4: //trade modified
								responseStatus = 3; //modified
								break;
							case 5: //trade expired, should not be reached
							case 6: //trade rejected by sender
							case 7:
								responseStatus = 2; //rejected
								break;
							case 8:
								//sometimes occurs even when items were transferred
								//if this happens, should be set to '3' in DB
								responseStatus = 2; //rejected
						}
						
						return "{\"id\": " + command.id + ",\"status\":" + responseStatus + "}";
					} else {
						// transaction is still active
						return "{\"id\": " + command.id + ",\"status\":5}"; //trade is pending
					}
				case 3:
					// item exchange
					SwapThread tradeSwapThread = new SwapThread(main, loadBalancer, browser, main.tradeStatusThread);
					tradeSwapThread.init(sqlthread, command);
					tradeValidity = tradeSwapThread.validItemExchange();
					switch (tradeValidity) {
						case VALID:
							// kick off thread to swap items
							Log.println(ThreadType.LISTENER, LogType.INFO, String.format(
									"Received valid item swap request from steamid: %s with: %s",
									command.user1, command.user2));
							tradeSwapThread.start();
							return "{\"result\": 1}"; //accepted
						case INVALID_LIST_1:
							Log.println(ThreadType.LISTENER, LogType.INFO, String.format(
									"Received invalid item swap request from steamid: %s with: %s. " +
									"Items missing from user1",
									command.user1, command.user2));
							return "{\"result\": 3}"; //items missing from user1
						case INVALID_LIST_2:
							Log.println(ThreadType.LISTENER, LogType.INFO, String.format(
									"Received invalid item swap request from steamid: %s with: %s. " +
									"Items missing from user2",
									command.user1, command.user2));
							return "{\"result\": 4}"; //items missing from user2
						default:
							Log.println(ThreadType.LISTENER, LogType.DEBUG, "Unknown result type: " + tradeValidity);
							break;
					}
					break;
				default:
					// should not be reached
					Log.println(ThreadType.LISTENER, LogType.INFO, "Unknown message id: " + command.msg);
					break;
			}
		} catch (JsonParseException e) {
			// tell server request was invalid
			return "{\"result\": 2}";
		}
		
		return response;
	}

	@Override
	public void run() {
		//TODO check secret key
		InputStream input = null;
		OutputStream output = null;
		BufferedReader in = null;
		DataOutputStream out = null;
		
		try {
			input = clientSocket.getInputStream();
			output = clientSocket.getOutputStream();
			
			in = new BufferedReader(new InputStreamReader(input));
			out = new DataOutputStream(output);
			
			String currentLine = in.readLine();
			String headerLine = currentLine;
			StringTokenizer tokenizer = new StringTokenizer(headerLine);
			String httpMethod = tokenizer.nextToken();
			String httpQueryString = tokenizer.nextToken();
			int contentLength = 0;
			
			if (httpMethod.equals("GET")) {
				Log.println(ThreadType.LISTENER, LogType.INFO, "Received GET request");
//				String response = main.sendStaticTrade();
				sendResponse(200, "hello world", out);
			} else if (httpMethod.equals("POST")) {
				Log.println(ThreadType.LISTENER, LogType.INFO, "Received POST request");
				
				// Get content length from headers
				while(in.ready()) {
					currentLine = in.readLine();
					if (currentLine.indexOf("Content-Length:") != -1) {
						contentLength = Integer.parseInt(currentLine.split("Content-Length: ")[1]);
						break;
					}
				}
				
				// 50 is some random high number
				for (int i = 0; i < 50; i++) {
					currentLine = in.readLine();
					if(currentLine == null) {
						break;
					} else if (currentLine.length() == 0) {
						char[] postData = new char[contentLength];
						in.read(postData, 0, contentLength);
						String postString = new String(postData, 0, contentLength);
						
						String response = handleMessage(postString);
						sendResponse(200, response, out);
					}
				}
			}
		} catch (IOException e) {
			Log.println(ThreadType.LISTENER, LogType.DEBUG, "Error when reading/writing from socket: " + e.getLocalizedMessage());
		} finally {
			try {
				output.close();
				input.close();
				in.close();
				out.close();
			} catch (IOException ioe) {
				Log.println(ThreadType.LISTENER, LogType.DEBUG, "Error when closing socket streams: " + ioe.getLocalizedMessage());
			}

		}
	}
}
