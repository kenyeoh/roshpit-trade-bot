package rtb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import json.WebCommand.WebAsset;
import rtb.Log.LogType;
import rtb.Log.ThreadType;
import trade.StatusThread;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class PostgreSQLThread extends Thread {

	private ArrayList<Connection> connections;
	private String hostName; //JDBC hostname. Format should be: jdbc:postgresql://hostname/databasename
	private String databaseName;
	private String itemTableName;
	private String transactionsTableName;
	private String username;
	private String password;
	private int refreshInterval; //interval between syncing with database

	private String host;
	private boolean initial;
	private boolean isRunning;

	private LoadBalancer loadBalancer;
	private StatusThread tradeStatusThread;

	public PostgreSQLThread(LoadBalancer loadBalancer, StatusThread tradeStatusThread) {
		this.loadBalancer = loadBalancer;
		this.tradeStatusThread = tradeStatusThread;
		initial = true;

		//load settings
		hostName = Config.configuration.getString("db_host_name");
		databaseName = Config.configuration.getString("db_database_name");
		itemTableName = Config.configuration.getString("db_item_table_name");
		transactionsTableName = Config.configuration.getString("db_transactions_table_name");
		username = Config.configuration.getString("db_username");
		password = Config.configuration.getString("db_password");
		refreshInterval = Config.configuration.getInt("db_refresh_item_map_interval", 3600);
		
		host = "jdbc:postgresql://" + hostName + "/" + databaseName;
	}

	public void init() {
		connections = new ArrayList<Connection>();

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "PostgreSQL driver not found: " + e.getLocalizedMessage());
		}
	}

	//gets a connection
	public Connection connection() {
		synchronized(connections) {
			if(connections.isEmpty()) {
				try {
					Log.println(ThreadType.SQLTHREAD, LogType.INFO, "Creating new connection...");
					connections.add(DriverManager.getConnection(host, username, password));
				} catch (SQLException e) {
					Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to connect to PostgreSQL database: " + e.getLocalizedMessage());
				}
			}

			//might still have no connections if creating a connection failed
			if(connections.isEmpty()) {
				return null;
			} else {
				Connection connection = connections.remove(0);
				try {
					if(connection.isClosed()) {
						return connection(); //return a new connection
					} else {
						return connection; //return this connection
					}
				} catch (SQLException e) {
					return connection(); //return a new connection
				}
			}
		}
	}
	
	public void connectionReady(Connection connection) {
		synchronized(connections) {
			connections.add(connection);
			Log.println(ThreadType.SQLTHREAD, LogType.INFO, "Recovering connection; now at " + connections.size() + " connections");
		}
	}
	
	public ArrayList<Item> getItemTable() {
		//k = botUsername, v = list of items
		ArrayList<Item> items = new ArrayList<Item>();
		String botUsername;
		long steamid;
		String original_id;
		int count = 0;
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + itemTableName);
			ResultSet result = statement.executeQuery();
			while(result.next()) {
				count++;
				botUsername = result.getString("bot_username");
				steamid = Long.parseLong(result.getString("steamid"));
				original_id = result.getString("original_id");
				
				Item newItem = new Item(botUsername, steamid, original_id);
//				// if this is the first time we've seen the username, make a new entry
//				if (itemMap.get(botUsername) == null) {
//					itemMap.put(botUsername, new ArrayList<Item>());
//				}
				// add the item to the list of items
				items.add(newItem);
			}
			Log.println(ThreadType.SQLTHREAD, LogType.INFO, "Loaded items table. Found " + count + " items");
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to get items table: " + e.getLocalizedMessage());
		}
		
		return items;
	}
	
	//TODO make this do multiple items
	public boolean addItem(String botUsername, long steamid, String original_id) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"INSERT INTO " + itemTableName + " (bot_username, steamid, original_id) VALUES (?, ?, ?)");
			statement.setString(1, botUsername);
			statement.setString(2, ((Long)steamid).toString());
			statement.setString(3, original_id);
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to add item " + original_id + ": " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	public boolean addItems(String botUsername, long steamid, List<WebAsset> assets) {
		try {
			Connection connection = connection();
			
			//build the multi row insert sql statement
			StringBuilder sql = new StringBuilder("INSERT INTO ");
			sql.append(itemTableName);
			sql.append(" (bot_username, steamid, original_id) VALUES (?, ?, ?)");
			
			int numberOfItems = assets.size();
			
			// we already have 1 row with values, so start at 1
			for (int i = 1; i < numberOfItems; i++) {
				sql.append(", (?, ?, ?)");
			}
			
			PreparedStatement statement = connection.prepareStatement(sql.toString());
			
			// set values
			for (int i = 0; i < numberOfItems; i++) {
				statement.setString(1 + i*3, botUsername);
				statement.setString(2 + i*3, ((Long)steamid).toString());
				statement.setString(3 + i*3, assets.get(i).original_id);
			}
			
			statement.execute();
			connectionReady(connection);
		}catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to add items " + assets + ": " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	//TODO make this do multiple items
	public boolean removeItem(String original_id) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"DELETE FROM " + itemTableName + " WHERE original_id=?");
			statement.setString(1, original_id);
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to remove item " + original_id + ": " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	public boolean removeItems(List<WebAsset> assets) {
		try {
			Connection connection = connection();
			
			//build the multi row insert sql statement
			StringBuilder sql = new StringBuilder("DELETE FROM ");
			sql.append(itemTableName);
			sql.append(" WHERE original_id=?");
			
			int numberOfItems = assets.size();
			
			// we already have 1 WHERE clause, so start at 1
			for (int i = 1; i < numberOfItems; i++) {
				sql.append(" OR original_id=?");
			}
			
			PreparedStatement statement = connection.prepareStatement(sql.toString());
			
			// set values
			for (int i = 0; i < numberOfItems; i++) {
				statement.setString(i + 1, assets.get(i).original_id); //value setting starts at 1
			}
			
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to remove items " + assets + ": " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	//TODO make this do multiple items
	public boolean updateItemOwner(String original_id, String steamid) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"UPDATE " + itemTableName + " SET steamid=? WHERE original_id=?");
			statement.setString(1, steamid);
			statement.setString(2, original_id);
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to update item " + original_id + " " + 
					"'s owner: " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	//TODO make this do multiple items
	public boolean updateItemOwnerAndBot(String original_id, String bot, String steamid) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"UPDATE " + itemTableName + " SET bot_username=?, steamid=? WHERE original_id=?");
			statement.setString(1, bot);
			statement.setString(2, steamid);
			statement.setString(3, original_id);
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to update item " + original_id + " " + 
					"'s owner and bot: " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	//TODO make this do multiple items
	public boolean updateItemBot(String original_id, String bot) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"UPDATE " + itemTableName + " SET bot_username=? WHERE original_id=?");
			statement.setString(1, bot);
			statement.setString(2, original_id);
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to update item " + original_id + " " + 
					"'s bot: " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	public ConcurrentHashMap<Integer, Transaction> getActiveTransactions() {
		//k = id, v = list of active transactions
		ConcurrentHashMap<Integer, Transaction> activeTransactions = new ConcurrentHashMap<Integer, Transaction>();
		int id;
		String me;
		String them;
		String bot_username;
		long steamid;
		String tradeofferid;
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM " + transactionsTableName + " WHERE status=2");
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				id = result.getInt("id");
				me = result.getString("me");
				them = result.getString("them");
				bot_username = result.getString("bot_username");
				steamid = Long.parseLong(result.getString("steamid"));
				tradeofferid = result.getString("tradeofferid");
				Transaction newTransaction = new Transaction(me, them, 1, bot_username, steamid, tradeofferid);
				activeTransactions.put(id, newTransaction);
			}
			Log.println(ThreadType.SQLTHREAD, LogType.INFO,
				"Loaded transactions table. Found " + activeTransactions.size() + " active transactions");
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to get transactions table: " + e.getLocalizedMessage());
		}
		
		return activeTransactions;
	}
	
	public Transaction getTransaction(int id) {
		Transaction transaction = null;
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM " + transactionsTableName + " WHERE id=" + id);
			ResultSet result = statement.executeQuery();
			
			//TODO make it not a while loop, but lazy to go find out how
			while (result.next()) {
				String me = result.getString("me");
				String them = result.getString("them");
				int status = result.getInt("status");
				String bot_username = result.getString("bot_username");
				long steamid = Long.parseLong(result.getString("steamid"));
				String tradeofferid = result.getString("tradeofferid");
				
				transaction = new Transaction(me, them, status, bot_username, steamid, tradeofferid);
			}
			connectionReady(connection);
		}  catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to get transaction id " + id + ": " + e.getLocalizedMessage());
		}
		
		return transaction;
	}
	
	public boolean addTransaction(int id, String me, String them, String bot_username, String steamid, String tradeofferid) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"INSERT INTO " + transactionsTableName + " (id, me, them, status, bot_username, steamid, tradeofferid) VALUES (?, ?, ?, ?, ?, ?, ?)");
			statement.setInt(1, id);
			statement.setString(2, me);
			statement.setString(3, them);
			statement.setInt(4, 2);
			statement.setString(5, bot_username);
			statement.setString(6, steamid);
			statement.setString(7, tradeofferid);
			statement.execute();
            connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to insert into " + transactionsTableName + " table: " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	public boolean updateTransaction(int id, int status) {
		try {
			Connection connection = connection();
			PreparedStatement statement = connection.prepareStatement(
				"UPDATE " + transactionsTableName + " SET status=? WHERE id=?");
			statement.setInt(1, status);
			statement.setInt(2, id);
			statement.execute();
			connectionReady(connection);
		} catch (SQLException e) {
			Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Unable to update status for id: " + id + " " + e.getLocalizedMessage());
			return false;
		}
		return true;
	}
	
	public void run() {
		isRunning = true; //flag for whether to continue running or stop
		int count = 0; //number of times we go through the run loop
		
		while(isRunning) {
			if(initial) {
				
			}
			
			count = count % refreshInterval;
			if(count == 0) {
				loadBalancer.setItemMap(getItemTable());
				tradeStatusThread.setActiveTransactions(getActiveTransactions());
			}
			count++;
			
			if(initial) {
				initial = false;
			}
			
			try {
				Thread.sleep(1000); //sleep for 1 second
			} catch (InterruptedException e) {
				Log.println(ThreadType.SQLTHREAD, LogType.DEBUG, "Sleep interrupted: " + e.getLocalizedMessage());
			}
		}
	}
}
