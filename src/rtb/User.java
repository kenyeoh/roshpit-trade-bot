package rtb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import rtb.Log.LogType;
import rtb.Log.ThreadType;
import rtb.Main.Account;

public class User {
	
	public String username;
	public String password;
	public Account account;
	public HashMap<String, String> cookies;
	public String cookieString;
	public String sessionID;
	
	public User(String username, String password, Account account, HashMap<String, String> cookies) {
		this.username = username;
		this.password = password;
		this.account = account;
		this.cookies = cookies;
		this.cookieString = getCookieString(cookies);
		this.sessionID = getSessionId(cookies);
	}
	
	public static String getCookieString(HashMap<String, String> cookies) {
		String cookieString = "";
        List<String> keys = new ArrayList<String>(cookies.keySet());
        for(String key: keys) {
            cookieString += key + "=" + cookies.get(key) + "; ";
        }
        if(!cookieString.equals("")) { //if cookieString is not empty, remove last semicolon
            cookieString = cookieString.substring(0, cookieString.length() - 2);
        }
        return cookieString;
	}
	
	public static String getSessionId(HashMap<String, String> cookies) {
		String sessionId = "";
		for(Entry<String, String> entry : cookies.entrySet()) {
			String cookie = entry.getKey();
			if(cookie.equals("sessionid")) {
				sessionId = entry.getValue();
				
				// Remove the semicolon
				int indexOfSemiColon = sessionId.indexOf(";");
				sessionId = sessionId.substring(0, indexOfSemiColon);
				
				return sessionId;
			}
		}
		Log.println(ThreadType.MAIN, LogType.DEBUG, "SessionID cookie not found. You may not be able to use the steam web functions");
		return sessionId;
	}
}
