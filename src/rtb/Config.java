package rtb;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import rtb.Log.LogType;
import rtb.Log.ThreadType;

public class Config {
	public static final String CONFIGURATION_FILE = "rtb.cfg";
	public static Configuration configuration;

	public static void load() {
		String config_file = CONFIGURATION_FILE;
		try {
			configuration = new PropertiesConfiguration(config_file);
		} catch(ConfigurationException e) {
			Log.println(ThreadType.CONFIG, LogType.DEBUG, "Error in loading config: " + e.getMessage());
		}
	}

	// Special get string that will return null if key is not found
	public static String getString(String key) {
		String s = configuration.getString(key, null);

		if(s == null || s.trim().equals("")) {
			return null;
		} else {
			return s;
		}
	}
}
