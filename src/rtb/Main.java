package rtb;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import javax.crypto.Cipher;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.DatatypeConverter;

import json.GetRsaKey;
import json.SteamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import rtb.Log.LogType;
import rtb.Log.ThreadType;
import trade.RejectThread;
import trade.StatusThread;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class Main {
	
	public static final String CAPTCHA_GID_URL = "https://steamcommunity.com/public/captcha.php?gid=";
	public static final String DO_LOGIN_URL = "https://steamcommunity.com/login/dologin/";
	public static final String REDIRECT_TO_HOME_URL = "http://steamcommunity.com/actions/RedirectToHome";
	public static final String WEB_TRADE_ELIGIBILITY_URL = "https://steamcommunity.com/market/eligibilitycheck/?goto=%2Fmarket%2F";
	public static final boolean REMEMBER_LOGIN = false;

	private Log log;
	protected Browser browser;
	private Gson gson;
	protected PostgreSQLThread sqlthread;
	protected LoadBalancer loadBalancer;
	protected StatusThread tradeStatusThread;
	private RejectThread rejectThread;
	private ListenServer listenServer;

	private boolean accountLogin;
	private int numberOfAccounts;
	private List<User> users;
	
	public static enum Account {
		// username, id, authentication (for steamguard), api key, partner, code
		BOT1("roshpit_bot1", "76561198163725523", "9E0D145994897D0D911D2A0752B7167DBBED6C88; ",
			 "089736910CA0A97B109C6AEE20F0743F", "203459795", "gdGVa0XJ"),
		BOT2("roshpit_bot2", "76561198163842394", "E06BFB1FF485914B9784DF7B4E623D6C85B70FB9; ",
			 "DDA1C37C5A2DA738C95ABD806BB58446", "203576666", "BLlWHQq2"),
		BOT3("roshpit_bot3", "76561198163735243", "13AA1E1A033F744E00F41A25D912F8323C6F87EF; ",
			 "C58DA466A1118F43F6684BC6A9D67FA2", "203469515", "kFjiXYAx");
//		XIII_DRAGON("xiii_dragon", "76561198038526109", "", "", "", "");

		private String username;
		private String id;
		private String auth;
		private String key;
		private String partner;
		private String token;

		private Account(String username, String id, String auth, String key, String partner, String token) {
			this.username = username;
			this.id = id;
			this.auth = auth;
			this.key = key;
			this.partner = partner;
			this.token = token;
		}

		public String getUsername() {
			return username;
		}

		public String getId() {
			return id;
		}
		
		public String getKey() {
			return key;
		}
		
		public String getPartner() {
			return partner;
		}
		
		public String getToken() {
			return token;
		}

		private boolean hasAuth() {
			if(auth.equals("")) {
				return false;
			} else {
				return true;
			}
		}
	}

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Main main = new Main();
	}

	public Main() {
		Config.load();
		log = new Log();
		log.init();
		browser = new Browser();
        gson = new Gson();
        users = new ArrayList<User>();
        loadBalancer = new LoadBalancer();

		// Load settings
		accountLogin = Config.configuration.getBoolean("account_login", false);
		numberOfAccounts = Config.configuration.getInt("number_accounts", 1);
		
		for(int i = 1; i < numberOfAccounts + 1; i++) {
			// Load user settings
			String username = Config.configuration.getString("username_" + i);
			String password = Config.configuration.getString("password_" + i);
			
			// Get Accounts based on username
			Account account = getAccount(username);
			
			// Get cookies
			HashMap<String, String> cookies = getCookies(account, password, accountLogin);
			
			// Create user object
			User newUser = new User(username, password, account, cookies);
			
			// Add user object to list of users
			users.add(newUser);
			
			loadBalancer.addBot(username);
		}
		
		tradeStatusThread = new StatusThread(this, loadBalancer, browser);
        tradeStatusThread.start();
        
        rejectThread = new RejectThread(this, browser);
        rejectThread.start();
		
		listenServer = new ListenServer(this, loadBalancer, tradeStatusThread, browser);
		new Thread(listenServer).start();
        
		initSQL();
	}
	
	public void initSQL() {
        sqlthread = new PostgreSQLThread(loadBalancer, tradeStatusThread);
        sqlthread.init();
        sqlthread.start();
        tradeStatusThread.setSQLThread(sqlthread);
        loadBalancer.setSQLThread(sqlthread);
        listenServer.setSQLThread(sqlthread);
    }
    
	public HashMap<String, String> getCookies(Account acc, String password, boolean login) {
		String cookieFileName = acc.username + "_cookies";
		HashMap<String, String> cookies = new HashMap<String, String>();
		if(login) {
			cookies = login(acc, password);
			saveCookies(cookies, cookieFileName);
		} else {
			cookies = loadCookies(cookieFileName);
		}
		return cookies;
	}
	
	public HashMap<String, String> login(Account acc, String password) {
        HashMap<String, String> cookieHashMap = new HashMap<String, String>();
        String cookieString = User.getCookieString(cookieHashMap);
        
        // If we know the auth ticket of the account in advance, add it to the cookies so
        // we don't have to enter steam guard code again
        if(acc.hasAuth()) {
            cookieHashMap.put("steamMachineAuth" + acc.id, acc.auth);
            cookieString = User.getCookieString(cookieHashMap);
        }
        
        Log.println(ThreadType.MAIN, LogType.INFO, "Attempting to login as " + acc.username);
		HashMap<String, String> usernameData = new HashMap<String, String>();
        usernameData.put("username", acc.username);
        
        // Get RSA key from steam
        String RSAresponse = browser.getRSAJSON(usernameData, cookieHashMap);
        
        // Parse RSA key to JSON
        GetRsaKey rsaJSON = gson.fromJson(RSAresponse, GetRsaKey.class);
        
        // Validate RSA key
        if(rsaJSON.success != true) {
        	Log.println(ThreadType.MAIN, LogType.DEBUG, "Invalid RSA key: " + RSAresponse);
            return null;
        }
        
        // Encrypt our password using the RSA key
        String encryptedPassword = "";
        try {
	        byte[] modulusBytes = HexToByte(rsaJSON.publickey_mod); //convert public key modulus from hex to byte array
	        byte[] exponentBytes = HexToByte(rsaJSON.publickey_exp); //convert public key exponent from hex to byte array
	        BigInteger modulus = new BigInteger(1, modulusBytes); //convert byte array to biginteter
	        BigInteger exponent = new BigInteger(1, exponentBytes); //convert byte array to biginteter
	        RSAPublicKeySpec rsaPubKey = new RSAPublicKeySpec(modulus, exponent); //generate rsa public key
	        
	        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	        PublicKey publicKey = keyFactory.generatePublic(rsaPubKey);
	        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
	        
	        byte[] bytePassword = password.getBytes(); //convert password to byte array
	        byte[] encodedPassword = cipher.doFinal(bytePassword); //encrypt password
	        String encryptedBase64Password = Base64.encodeBase64String(encodedPassword); //base64
	        encryptedPassword = URLEncoder.encode(encryptedBase64Password, "UTF-8"); //formatting
		} catch (Exception e) {
			Log.println(ThreadType.MAIN, LogType.DEBUG, "Error when encrypting password: " + e.getLocalizedMessage());
			return null;
		}
        
        HashMap<String, String> loginData = new HashMap<String, String>();
        loginData.put("username", acc.username);
        loginData.put("password", encryptedPassword);
        
    	// Time
        String time = "";
        try {
            time = new URI(rsaJSON.timestamp).toASCIIString();
        } catch (URISyntaxException e) {
        	Log.println(ThreadType.MAIN, LogType.DEBUG, "Error when getting timestamp: " + e.getLocalizedMessage());
        	return null;
        }
        loginData.put("rsatimestamp", time);
        
        // Remember login
        if(REMEMBER_LOGIN) {
        	loginData.put("remember_login", "true");
        } else {
        	loginData.put("remember_login", "false");
        }
        
        SteamResult loginJson = null;
        boolean needInformation = true;
        while(needInformation) {
            System.out.println("Steam web: logging in...");
            // On first loop, loginJson is null, and these are false
            boolean captcha = (loginJson != null) && (loginJson.captcha_needed == true); //brackets not necessary, but easier to read
            boolean steamGuard = (loginJson != null) && (loginJson.emailauth_needed == true); //brackets not necessary, but easier to read
            
            // Captcha
            String captchaText = "";
            String captchaGID = "";
            if(captcha) {
                System.out.println("Steam web: Captcha is needed.");
                captchaGID = loginJson.captcha_gid; //update captchaGID
                openWebpage(CAPTCHA_GID_URL + loginJson.captcha_gid); //open captcha in web browser
                System.out.print("Steam web: type the captcha:");
                Scanner sc = new Scanner(System.in);
                captchaText = sc.nextLine(); //get typed captcha
                sc.close();
                try {
                    captchaText = URLEncoder.encode(captchaText, "UTF-8"); //formatting
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            loginData.put("captchagid", captchaGID);
            loginData.put("captcha_text", captchaText);
            
            // Steam Guard
            String steamGuardText = "";
            String steamGuardId = "";
            if(steamGuard) {
                System.out.println("Steam web: Steam Guard is needed.");
                System.out.print("Steam web: type the code:");
                Scanner sc = new Scanner(System.in);
                steamGuardText = sc.nextLine(); //get typed Steam Guard code
                sc.close();
                steamGuardId = loginJson.emailsteamid; //update email steam ID
            }
            loginData.put("emailauth", steamGuardText);
            loginData.put("emailsteamid", steamGuardId);
            
            // Send query
            HttpURLConnection connection = browser.getHTTPPostConnection(DO_LOGIN_URL, loginData, cookieString, true);
            String response = readHttpResponse(connection);
            
            loginJson = gson.fromJson(response.toString(), SteamResult.class); //update loginJson
            
            // Get cookies
            List<String> cookieList = connection.getHeaderFields().get("Set-Cookie");
            if(cookieList != null) { //if cookies exist
                for(String cookieTemp : cookieList) { //iterate through list of cookies
                    int indexOfEquals = cookieTemp.indexOf("=");
                    cookieHashMap.put(cookieTemp.substring(0, indexOfEquals), cookieTemp.substring(indexOfEquals + 1, cookieTemp.length()));
                    cookieString = User.getCookieString(cookieHashMap); //update cookieString as well
                }
            }
            
            // Get session ID cookies
            HashMap<String, String> emptyData = new HashMap<String, String>(); //need it for request method
            HttpURLConnection connection2 = browser.getHTTPPostConnection(REDIRECT_TO_HOME_URL, emptyData, cookieString, true);
            cookieList = connection2.getHeaderFields().get("Set-Cookie");
            if(cookieList != null) { //if cookies exist
                for(String cookieTemp : cookieList) { //iterate through list of cookies
                    int indexOfEquals = cookieTemp.indexOf("=");
                    cookieHashMap.put(cookieTemp.substring(0, indexOfEquals), cookieTemp.substring(indexOfEquals + 1, cookieTemp.length())); //does not include '='
                    cookieString = User.getCookieString(cookieHashMap); //update cookieString as well
                }
            }
            
            // Get webTradeEligibility cookie
            HttpURLConnection connection3 = browser.getHTTPPostConnection(WEB_TRADE_ELIGIBILITY_URL, emptyData, cookieString, true);
            cookieList = connection3.getHeaderFields().get("Set-Cookie");
            if(cookieList != null) { //if cookies exist
                for(String cookieTemp : cookieList) { //iterate through list of cookies
                    int indexOfEquals = cookieTemp.indexOf("=");
                    cookieHashMap.put(cookieTemp.substring(0, indexOfEquals), cookieTemp.substring(indexOfEquals + 1, cookieTemp.length())); //does not include '='
                    cookieString = User.getCookieString(cookieHashMap); //update cookieString as well
                }
            }
            
            // Check if we need captcha or Steam Guard. Loops if we still need them
            if(loginJson.captcha_needed == false && loginJson.emailauth_needed == false) {
                needInformation = false;
            }
        }
        
        if(loginJson.success == true) {
            browser.submitCookies(cookieString);
        }
        
        return cookieHashMap;
	}
	
	public void saveCookies(HashMap<String, String> cookies, String fileName) {
        try {
            PrintWriter writer = new PrintWriter(System.getProperty("user.dir") + "/" + fileName + ".txt", "UTF-8");
            List<String> keys = new ArrayList<String>(cookies.keySet());
            for(String key : keys) { //for each key in keyset in cookies
                writer.println(key + "=" + cookies.get(key));
            }
            writer.close();
            Log.println(ThreadType.MAIN, LogType.INFO, "Successfully saved cookies to file");
        } catch (Exception e) {
        	Log.println(ThreadType.MAIN, LogType.DEBUG, "Error when saving cookies: " + e.getLocalizedMessage());
        }
    }
    
    public HashMap<String, String> loadCookies(String fileName) {
        HashMap<String, String> cookieHashMap = new HashMap<String, String>();
        try {
            List<String> lines = FileUtils.readLines(new File(System.getProperty("user.dir") + "/" + fileName + ".txt"), "utf-8");
            if(lines != null) { //if file is not empty
                for(String line : lines) { //iterate through list of cookies
                    int indexOfEquals = line.indexOf("=");
                    cookieHashMap.put(line.substring(0, indexOfEquals), line.substring(indexOfEquals + 1, line.length()));
                }
            }
            Log.println(ThreadType.MAIN, LogType.INFO, "Successfully loaded " + fileName + " from file");
        } catch (IOException e) {
        	Log.println(ThreadType.MAIN, LogType.DEBUG, "Error when loading cookies " + e.getLocalizedMessage());
        }
        return cookieHashMap;
    }
    
    public User getUser(String username) {
    	for (User user : users) {
    		if (user.username.equals(username)) {
    			return user;
    		}
    	}
    	
    	return null;
    }

	/**
	 * Returns the Account that matches the username
	 * If no Account matches, returns null
	 * 
	 * @param  username the username of the account we are looking up
	 * @return          the Account that matches the username
	 */
	public Account getAccount(String username) {
		for(Account acc : Account.values()) {
			if(acc.username.equals(username)) {
				return acc;
			}
		}
		Log.println(ThreadType.MAIN, LogType.DEBUG, "No matching Account for username: " + username);
		return null;
	}
	
	public String readHttpResponse(HttpURLConnection connection) {
        String result = "";
        try {
            // Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer(); 
            while((line = rd.readLine()) != null) {
              response.append(line);
              response.append('\r');
            }
            rd.close();
            result = response.toString();
        } catch(Exception e) {
        	Log.println(ThreadType.MAIN, LogType.DEBUG, "Error reading HTTP response: " + e.getLocalizedMessage());
        }
        return result;
    }
	
	public String readHttpsResponse(HttpsURLConnection connection) {
        String body = "";
        try {
            InputStream in;
            if(connection.getResponseCode() == 200) {
                in = connection.getInputStream();
            } else {
                in = connection.getInputStream();
            }
            
            // Change to gzip format if required
            String contentEncoding = connection.getContentEncoding();
            if (contentEncoding != null) {
            	if(connection.getContentEncoding().equals("gzip")) {
	    			in = new GZIPInputStream(in);
	    		}
            }
            
    		body = IOUtils.toString(in);
        } catch (Exception e) {
        	Log.println(ThreadType.MAIN, LogType.DEBUG, "Error reading HTTPS response: " + e.getLocalizedMessage());
        }
        return body;
    }
	
	public static byte[] HexToByte(String hex) throws Exception {
        if(hex.length() % 2 == 1) {
            throw new Exception("The binary key cannot have an odd number of digits");
        }
        return DatatypeConverter.parseHexBinary(hex);
    }
	
	public static void openWebpage(String uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URI(uri));
            } catch (Exception e) {
            	Log.println(ThreadType.MAIN, LogType.DEBUG, "Error opening: " + e.getLocalizedMessage());
            }
        }
    }
}
