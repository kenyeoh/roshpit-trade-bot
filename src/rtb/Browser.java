package rtb;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import rtb.Log.LogType;
import rtb.Log.ThreadType;

/**
 * 
 * @author Ken Neth Yeoh
 * 
 */
public class Browser {

	public static final String LOGIN_GET_RSA_URL = "https://steamcommunity.com/login/getrsakey";
	public static final String STEAM_URL = "https://steamcommunity.com/";
	public static final String STEAM_MARKET_REFERER = "https://steamcommunity.com/login/home/?goto=market%2F%3Fl%3Denglish";
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";
	public static final String ACCEPT_TRADE_OFFER = "https://steamcommunity.com/tradeoffer/%s%s";
	
	public Browser() {
		// Create the SSL connection
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, null, new java.security.SecureRandom());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}

	public String getRSAJSON(HashMap<String, String> usernameData, HashMap<String, String> cookieHashMap) {
		String rsa = "";
		try {
			Response response = Jsoup
					.connect(LOGIN_GET_RSA_URL)
					.ignoreContentType(true)
					.data(usernameData)
					.cookies(cookieHashMap)
					.method(Method.POST)
					.execute();
			Document doc = response.parse();
			rsa = doc.body().text(); //only want body
		} catch (IOException e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Unable to get RSA key from steam: " + e.getLocalizedMessage());
			return null;
		}
		return rsa;
	}
	
	public HttpURLConnection getHTTPPostConnection(String url, HashMap<String, String> data, String cookies, boolean ajax) {
		HttpURLConnection connection = null;
		try {
			URL iurl = new URL(url);
			connection = (HttpURLConnection)iurl.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			connection.setRequestProperty("Host", "steamcommunity.com");
			connection.setRequestProperty("User-Agent", USER_AGENT);
			connection.setRequestProperty("Referer", STEAM_MARKET_REFERER);
			connection.setRequestProperty("Connection", "close") ;

			if(ajax) {
				connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
				connection.setRequestProperty("X-Prototype-Version", "1.7");
			}
			connection.setRequestProperty("Cookie", cookies);

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Format dataString
			String dataString = data.toString();
			String formatedDataString = dataString.substring(1, dataString.toString().length() - 1).replaceAll(", ", "&");
			byte[] dataBytes = formatedDataString.getBytes();

			connection.setRequestProperty("Content-Length", "" + dataBytes.length);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.write(dataBytes, 0, dataBytes.length);
			wr.flush();
			wr.close();
		} catch (Exception e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Error when requesting URL: " + e.getLocalizedMessage());
		}

		return connection;
	}
	
	public HttpsURLConnection get(String url) {
		HttpsURLConnection secureConnection = null;
		try {
			URL iurl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection)iurl.openConnection();
			secureConnection = (HttpsURLConnection) connection;
			
			secureConnection.setRequestMethod("GET");
			secureConnection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			secureConnection.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch");
			secureConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			secureConnection.setRequestProperty("Cache-Control", "max-age=0");
			secureConnection.setRequestProperty("Connection", "keep-alive");
			secureConnection.setRequestProperty("DNT", "1");
			secureConnection.setRequestProperty("Host", "api.steampowered.com");
			secureConnection.setRequestProperty("User-Agent", USER_AGENT);
			
			secureConnection.connect();
			
		} catch (Exception e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Error when requesting https: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		return secureConnection;
	}

	/**
	 * Used for sending a trade request
	 * @param url
	 * @param referer
	 * @param dataString
	 * @param cookies
	 * @param sessionId
	 * @return
	 */
	public HttpsURLConnection sendTrade(String url, String referer, String dataString, String cookies) {
		HttpsURLConnection secureConnection = null;
		
        byte[] dataBytes = dataString.getBytes();

		try {
			URL iurl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection)iurl.openConnection();
			secureConnection = (HttpsURLConnection) connection;

			secureConnection.setRequestMethod("POST");
			secureConnection.setRequestProperty("Accept", "*/*");
			secureConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
			secureConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
//			secureConnection.setRequestProperty("Cache-Control", "max-age=0");
			secureConnection.setRequestProperty("Connection", "keep-alive");
			secureConnection.setRequestProperty("Content-Length", "" + dataBytes.length);
			secureConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			secureConnection.setRequestProperty("Cookie", cookies);
			secureConnection.setRequestProperty("DNT", "1");
			secureConnection.setRequestProperty("Host", "steamcommunity.com");
			secureConnection.setRequestProperty("Referer", referer);
			secureConnection.setRequestProperty("User-Agent", USER_AGENT);
			secureConnection.setRequestProperty("Origin", "https://steamcommunity.com");

			secureConnection.setUseCaches(false);
            secureConnection.setDoInput(true);
            secureConnection.setDoOutput(true);
			
			DataOutputStream wr = new DataOutputStream(secureConnection.getOutputStream());
            wr.write(dataBytes, 0, dataBytes.length);
            wr.flush();
            wr.close();
		} catch (Exception e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Error when requesting https: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		return secureConnection;
	}
	
	public HttpsURLConnection sendTradeResponse(String url, String dataString) {
		HttpsURLConnection secureConnection = null;
		
        byte[] dataBytes = dataString.getBytes();
        try {
			URL iurl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection)iurl.openConnection();
			secureConnection = (HttpsURLConnection) connection;
			secureConnection.setRequestMethod("POST");
			secureConnection.setRequestProperty("Content-Length", "" + dataBytes.length);
			
			secureConnection.setUseCaches(false);
            secureConnection.setDoInput(true);
            secureConnection.setDoOutput(true);
            
            DataOutputStream wr = new DataOutputStream(secureConnection.getOutputStream());
            wr.write(dataBytes, 0, dataBytes.length);
            wr.flush();
            wr.close();
        } catch (Exception e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Error when requesting https: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		return secureConnection;
	}
	
	public HttpsURLConnection sendTradeAccept(String tradeofferid, String formData, User user) {
		HttpsURLConnection secureConnection = null;
		
        byte[] dataBytes = formData.getBytes();

		try {
			URL iurl = new URL(String.format(ACCEPT_TRADE_OFFER, tradeofferid, "/accept"));
			HttpURLConnection connection = (HttpURLConnection)iurl.openConnection();
			secureConnection = (HttpsURLConnection) connection;

			secureConnection.setRequestMethod("POST");
			secureConnection.setRequestProperty("Accept", "*/*");
			secureConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
			secureConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			secureConnection.setRequestProperty("Connection", "keep-alive");
			secureConnection.setRequestProperty("Content-Length", "" + dataBytes.length);
			secureConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			secureConnection.setRequestProperty("Cookie", user.cookieString);
			secureConnection.setRequestProperty("DNT", "1");
			secureConnection.setRequestProperty("Host", "steamcommunity.com");
			secureConnection.setRequestProperty("Referer", String.format(ACCEPT_TRADE_OFFER, tradeofferid, ""));
			secureConnection.setRequestProperty("User-Agent", USER_AGENT);
			secureConnection.setRequestProperty("Origin", "https://steamcommunity.com");

			secureConnection.setUseCaches(false);
            secureConnection.setDoInput(true);
            secureConnection.setDoOutput(true);
			
			DataOutputStream wr = new DataOutputStream(secureConnection.getOutputStream());
            wr.write(dataBytes, 0, dataBytes.length);
            wr.flush();
            wr.close();
		} catch (Exception e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Error when requesting https: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		return secureConnection;
	}

	public void submitCookies(String cookies) {
		HttpURLConnection connection = null;
		String formattedCookieString = "";
		try {
			formattedCookieString = URLEncoder.encode(cookies, "UTF-8"); //formatting
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			URL iurl = new URL(STEAM_URL);
			connection = (HttpURLConnection)iurl.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Cookie", formattedCookieString);
			connection.connect();
		} catch(Exception e) {
			Log.println(ThreadType.BROWSER, LogType.DEBUG, "Error when submitting cookies: " + e.getLocalizedMessage());
		}
	}

}
