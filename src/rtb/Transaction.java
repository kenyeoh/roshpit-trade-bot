package rtb;

// matches the DB
public class Transaction {
	public String me;
	public String them;
	public int status;
	public String bot_username;
	public long steamid;
	public String tradeofferid;
	
	public Transaction(String me, String them, int status, String bot_username, long steamid, String tradeofferid) {
		this.me = me;
		this.them = them;
		this.status = status;
		this.bot_username = bot_username;
		this.steamid = steamid;
		this.tradeofferid = tradeofferid;
	}
}
