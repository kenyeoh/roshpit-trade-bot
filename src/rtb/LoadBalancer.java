package rtb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import rtb.Log.LogType;
import rtb.Log.ThreadType;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class LoadBalancer {
	
	public static int MAX_DOTA_INVENTORY_SIZE = 720;
	public static int INVENTORY_MAX = 700;
	
	private HashMap<String, ArrayList<Item>> botItemMap; //k = bot username, v = list of items
	private HashMap<Long, ArrayList<Item>> userItemMap; //k = steamid of owner, v = list of items
	private ArrayList<String> bots;
	private PostgreSQLThread sqlthread;
	
	public LoadBalancer() {
		botItemMap = new HashMap<String, ArrayList<Item>>();
		userItemMap = new HashMap<Long, ArrayList<Item>>();
		bots = new ArrayList<String>();
	}

	public void setSQLThread(PostgreSQLThread sqlthread) {
		this.sqlthread = sqlthread;
	}
	
    public void setItemMap(ArrayList<Item> items) {
    	HashMap<String, ArrayList<Item>> tempBotItemMap = new HashMap<String, ArrayList<Item>>();
    	HashMap<Long, ArrayList<Item>> tempUserItemMap = new HashMap<Long, ArrayList<Item>>();
    	for (Item item : items) {
//    		// check that the bot exists (optional)
//    		if (main.getUser(item.botUsername) == null) {
//    			Log.println(ThreadType.BALANCER, LogType.INFO, "Bot: " + item.botUsername + " does not exist");
//    		}
    		
    		// add the item to the botItemMap
    		// if this is the first time we've seen the username, make a new entry
    		if (tempBotItemMap.get(item.botUsername) == null) {
    			tempBotItemMap.put(item.botUsername, new ArrayList<Item>());
    		}
    		// add the item to the list of items
    		tempBotItemMap.get(item.botUsername).add(item);
    		
    		// add the item to the userItemMap
    		// if this is the first time we've seen the username, make a new entry
    		if (tempUserItemMap.get(item.steamid) == null) {
    			tempUserItemMap.put(item.steamid, new ArrayList<Item>());
    		}
    		// add the item to the list of items
    		tempUserItemMap.get(item.steamid).add(item);
    	}
    	
    	// update maps
    	botItemMap = tempBotItemMap;
    	userItemMap = tempUserItemMap;
    	
    	// add bots to map of bots, in the case the bot is empty and not listed in DB
    	for (String bot : bots) {
    		ArrayList<Item> botItems = botItemMap.get(bot);
    		if (botItems == null) {
    			botItems = new ArrayList<Item>();
    			botItemMap.put(bot, botItems);
    		}
    	}
    	
    }
    
    public boolean canSpaceBeMade(String botUsername, int space) {
    	int availableSpace = getAvailableSpace(botUsername);
    	
    	// if there is already enough space
    	if (availableSpace >= space) {
    		return true;
    	} else {
    		String mostEmptyBot = mostEmptyBot();
    		int spaceOnMostEmptyBot = getAvailableSpace(mostEmptyBot);
    		if (spaceOnMostEmptyBot > space) {
    			return true;
    		} else {
    			return false;
    		}
    	}
    }
    
    public boolean makeSpaceOnBot(String botUsername, int space) {
    	int availableSpace = getAvailableSpace(botUsername);
    	
    	// if there is already enough space
    	if (availableSpace >= space) {
    		return true;
    	} else {
    		// we need to make room on this bot
    		int spaceNeeded = space - availableSpace;
    		
    		// find which items to move, must move a user's entire list of items at a time
    		ArrayList<Item> itemsToMove = new ArrayList<Item>();
    		for (Entry<Long, ArrayList<Item>> entry : userItemMap.entrySet()) {
    			// if this user's items are being stored on this bot
    			ArrayList<Item> userItems = entry.getValue();
    			if (userItems.get(0).botUsername.equals(botUsername)) {
    				itemsToMove.addAll(userItems);
    				if (itemsToMove.size() > spaceNeeded) {
    					break;
    				}
    			}
    		}
    		
    		// possible to not be able to move enough items if space > botmaxspace
    		
    		// check if there is enough space on the most empty bot to receive the items
    		String mostEmptyBot = mostEmptyBot();
    		int spaceOnMostEmptyBot = getAvailableSpace(mostEmptyBot);
    		if (spaceOnMostEmptyBot < itemsToMove.size()) {
    			// not possible to move items to make space
    			return false;
    		}
    		
    		//TODO actually move items here
    		// for now assume it was accepted
		    boolean accepted = true;
		    
		    if (accepted) {
		    	// for each item we want to move
		    	for (Iterator<Item> iterator = itemsToMove.iterator(); iterator.hasNext();) {
		    		Item item = iterator.next();
		    		
		    		// update DB
		    		if (sqlthread.updateItemBot(item.original_id, mostEmptyBot)) {
		    			// update item's bot
		    			item.botUsername = mostEmptyBot;
		    			
		    			// remove item from original bot
		    			botItemMap.get(botUsername).remove(item);
		    			
		    			// add item to new bot
		    			botItemMap.get(mostEmptyBot).add(item);
		    		}
		    	}
		    	
		    	return true;
		    } else {
		    	return false;
		    }
    	}
    }
    
    /**
     * 
     * @return bot with highest amount of storage
     */
    public String mostEmptyBot() {
		String mostEmptyBot = "";
		int highestSpace = 0;
		for (Map.Entry<String, ArrayList<Item>> entry : botItemMap.entrySet()) {
		    ArrayList<Item> botItems = entry.getValue();
		    int space = getAvailableSpace(botItems);
		    Log.println(ThreadType.BALANCER, LogType.DEBUG, entry.getKey() + " has " + space + " empty slots");
		    if (highestSpace < space) {
		    	highestSpace = space;
		    	mostEmptyBot = entry.getKey();
		    }
		}
		return mostEmptyBot;
    }
    
    /**
     * 
     * @param steamid
     * @return name of the bot to deposit items to
     */
    public String getDepositBot(String steamid) {
    	try {
    		// get the bot which is currently holding items for this user
	    	Long steamidlong = Long.parseLong(steamid);
	    	List<Item> userItems = userItemMap.get(steamidlong);
	    	
	    	// we are not storing any of this user's items
	    	if (userItems == null || userItems.size() == 0) {
	    		return mostEmptyBot();
	    	} else {
	    		// return the bot storing this user's items
	    		return userItems.get(0).botUsername;
	    	}
    	} catch (NumberFormatException e) {
    		Log.println(ThreadType.BALANCER, LogType.INFO, "Steam id given is empty");
    		return "";
    	}
    	
    }
    
    /**
     * Finds which bot is storing this user's items
     * 
     * @param steamid
     * @return name of the bot holding this user's items
     */
    public String getWithdrawBot(String steamid) {
    	// get the bot which is currently holding items for this user
    	Long steamidlong = Long.parseLong(steamid);
    	List<Item> userItems = userItemMap.get(steamidlong);
    	
    	// we are not storing any of this user's items
    	if (userItems == null || userItems.size() == 0) {
    		Log.println(ThreadType.BALANCER, LogType.INFO, "Cannot find any bot holding items for steamid: " + steamid);
        	return null;
    	} else {
    		// return the bot storing this user's items
    		return userItems.get(0).botUsername;
    	}
    }
    
    public ArrayList<Item> getUsersItems(String steamid) {
    	// get the bot which is currently holding items for this user
    	Long steamidlong = Long.parseLong(steamid);
    	
    	ArrayList<Item> userItems = userItemMap.get(steamidlong);
    	
    	//work around if user exists, but has no items
    	if (userItems != null) {
    		if (userItems.size() == 0) {
    			return null;
    		}
    	}
    	
    	return userItems;
    }
    
    public void addUserItems(Long steamid, ArrayList<Item> items) {
    	userItemMap.put(steamid, items);
    }
    
    public Item getItem(String original_id) {
    	// searches the entirety of items to find original_id
    	for (Map.Entry<String, ArrayList<Item>> entry : botItemMap.entrySet()) {
    		for (Item item : entry.getValue()) {
    			if (item.original_id.equals(original_id)) {
    				return item;
    			}
    		}
    	}
    	
    	return null;
    }
    
    public void addItem(Item item) {
    	// add item to bot item map
    	ArrayList<Item> botItems = botItemMap.get(item.botUsername);
    	// create new entry in map if not exist
    	if (botItems == null) {
    		botItems = new ArrayList<Item>();
    		botItemMap.put(item.botUsername, botItems);
    	}
    	botItems.add(item);
    	
    	//add item to user item map
    	ArrayList<Item> userItems = userItemMap.get(item.steamid);
    	if (userItems == null) {
    		userItems = new ArrayList<Item>();
    		userItemMap.put(item.steamid, userItems);
    	}
    	userItems.add(item);
    }
    
    public void removeItem(String botUsername, long steamid, String original_id) {
    	// remove item from bot item map
    	ArrayList<Item> botItems = botItemMap.get(botUsername);
    	for (Item item : botItems) {
    		if (item.original_id.equals(original_id)) {
    			//found the item, remove it from bot items
    			botItems.remove(item);
    			break;
    		}
    	}
    	
    	// remove item from user item map
    	ArrayList<Item> userItems = userItemMap.get(steamid);
    	for (Item item : userItems) {
    		if (item.original_id.equals(original_id)) {
    			//found the item, remove it from user items
    			userItems.remove(item);
    			break;
    		}
    	}
    }
    
    public void addBot(String botUsername) {
    	if (!bots.contains(botUsername)) {
    		bots.add(botUsername);
    	}
    }
    
    public int getAvailableSpace(List<Item> items) {
    	return INVENTORY_MAX - items.size();
    }
    
    public int getAvailableSpace(String botUsername) {
    	ArrayList<Item> botItems = botItemMap.get(botUsername);
    	return INVENTORY_MAX - botItems.size();
    }
}
