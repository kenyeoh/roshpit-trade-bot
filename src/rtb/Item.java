package rtb;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class Item {
	
	//TODO sort by steamid
	public String botUsername;
	public long steamid; //owner
	public String original_id; // unique item id
	
	public Item(String botUsername, long steamid, String original_id) {
		this.botUsername = botUsername;
		this.steamid = steamid;
		this.original_id = original_id;
	}
}
