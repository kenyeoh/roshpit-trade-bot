package rtb;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class Log {
	
	public static final String LOG_FOLDER = "/log/";
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

	// These could be called logEnabled, logMaster, logDebug etc
	private boolean enabled;
	private boolean master;
	private boolean debug;
	
	private static boolean showDebug;
	
	private static PrintWriter master_out;
	private static PrintWriter debug_out;
	
	public static enum LogType {
		INFO, DEBUG
	}
	
	public static enum ThreadType {
		MAIN("[Main]"), CONFIG("[Config]"), BROWSER("[Browser]"),
		LISTENER("[Listener]"), SQLTHREAD("[SQLThread]"), BALANCER("[Balancer]"),
		TRADER("[Trader]");
		
		private String name;
		
		private ThreadType(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
	}
	
	public Log() {
		// Load settings
		enabled = Config.configuration.getBoolean("log_enabled", true);
		master = Config.configuration.getBoolean("log_master", true);
		debug = Config.configuration.getBoolean("log_debug", true);
		showDebug = Config.configuration.getBoolean("show_debug", true);
	}
	
	public void init() {
		// If logging is enabled
		if(enabled) {
			String logDirectory = System.getProperty("user.dir") + LOG_FOLDER; //this is where we store our log files
			
			// Create log folder if it doesn't already exist
            File logDirectoryFolder = new File(logDirectory);
            if(!logDirectoryFolder.exists()) {
                logDirectoryFolder.mkdir();
            }
            
            // Init printwriters
            try {
            	if(master) {
            		master_out = new PrintWriter(new FileWriter(logDirectory + "master.txt", true), true);
            	}
            	if(debug) {
            		debug_out = new PrintWriter(new FileWriter(logDirectory + "debug.txt", true), true);
            	}
            } catch(IOException e) {
            	System.out.println("[Main] Error initializing PrintWriters for logging");
            }
		}
	}
	
	public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(new Date());
    }
	
	public static void println(ThreadType threadType, LogType logType, String message) {
		String formattedMessage = String.format("[%s] %-11s %s", getDate(), threadType.name, message); //String to be printed to file
		
		// Print message to console. Only print debug statements to console if enabled
		if(logType == LogType.DEBUG) {
			if(showDebug) {
				System.out.println(formattedMessage);
			}
		} else {
			System.out.println(formattedMessage);
		}
		
		// Print to file depending on what type of message it is
		switch (logType) {
			case DEBUG:
				if(debug_out != null) {
					debug_out.println(formattedMessage);
				}
				break;
			case INFO:
				// We don't have a special log file for info; it should be printed in master
				break;
		}
		
		// Print to master log file if enabled
		if(master_out != null) {
			master_out.println(formattedMessage);
		}
	}
}
