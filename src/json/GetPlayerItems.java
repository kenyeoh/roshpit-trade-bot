package json;

import java.util.List;

public class GetPlayerItems {
	public Result result;
	
	public class Result {
		public String status;
		public int num_backpack_slots;
		public List<Item> items;
	}
	
	public class Item {
		public long id;
		public long original_id;
	}
	
}
