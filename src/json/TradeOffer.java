package json;

import java.util.ArrayList;
import java.util.List;

public class TradeOffer {
	public boolean newversion;
	public int version;
	public Person me;
	public Person them;
	
	public TradeOffer(boolean newVersion, int version) {
		this.newversion = newVersion;
		this.version = version;
		me = new Person(new ArrayList<String>(), false);
		them = new Person(new ArrayList<String>(), false);
	}
	
	public void addAssetMe(int appid, String contextid, int amount, String assetid) {
		Asset newAsset = new Asset(appid, contextid, amount, assetid);
		me.assets.add(newAsset);
	}
	
	public void addAssetThem(int appid, String contextid, int amount, String assetid) {
		Asset newAsset = new Asset(appid, contextid, amount, assetid);
		them.assets.add(newAsset);
	}
	
	public class Person {
		public List<Asset> assets;
		public List<String> currency; //not sure what type this is
		public boolean ready;
		
		public Person(List<String> currency, boolean ready) {
			assets = new ArrayList<Asset>();
			this.currency = currency;
			this.ready = ready;
		}
	}
	
	public class Asset {
		public int appid;
	    public String contextid;
	    public int amount;
	    public String assetid;
	    
	    public Asset(int appid, String contextid, int amount, String assetid) {
	    	this.appid = appid;
	    	this.contextid = contextid;
	    	this.amount = amount;
	    	this.assetid = assetid;
	    }
	}
}
