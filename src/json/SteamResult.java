package json;

public class SteamResult {
    public boolean success;
    public String message;
    public boolean captcha_needed;
    public String captcha_gid;
    public boolean emailauth_needed;
    public String emailsteamid;
}
