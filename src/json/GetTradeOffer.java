package json;

import java.util.List;

public class GetTradeOffer {
	public Response response;
	
	public class Response {
		public Offer offer;
		public Descriptions descriptions;
	}
	
	public class Offer {
		public String tradeofferid;
		public String accountid_other;
		public String message;
		public int expiration_time;
		public int trade_offer_state;
		public List<Item> items_to_give;
		public List<Item> items_to_receive;
		public boolean is_our_offer;
		public int time_created;
		public int time_updated;
	}
	
	public class Item {
		public String appid;
		public String contextid;
		public String assetid;
		public String classid;
		public String instanceid;
		public String amount;
		public boolean missing;
	}
	
	public class Descriptions {
		public int appid;
		public String classid;
		public String instanceid;
		public boolean currency;
		public String background_color;
		public String icon_url;
		public String icon_url_large;
		public List<Description> descriptions;
		public boolean tradable;
		public String name;
		public String name_color;
		public String type;
		public String market_name;
		public String market_hash_name;
		public boolean commodity;
	}
	
	public class Description {
		public String type;
		public String value;
		public String color;
	}
}
