package json;

import java.util.List;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class GetTradeOffers {
	public Response response;
	
	public class Response {
		public List<Offer> trade_offers_sent;
		public List<Offer> trade_offers_received;
	}
	
	public class Offer {
		public String tradeofferid;
		public String accountid_other;
		public String message;
		public int expiration_time;
		public int trade_offer_state;
		public List<Item> items_to_give;
		public List<Item> items_to_receive;
		public boolean is_our_offer;
		public int time_created;
		public int time_updated;
	}
	
	public class Item {
		public String appid;
		public String contextid;
		public String assetid;
		public String classid;
		public String instanceid;
		public String amount;
		public boolean missing;
	}
}
