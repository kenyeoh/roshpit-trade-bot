package json;

import java.util.List;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class WebCommand {
	public int msg;
	public String steamid; //steamid of person we are trading with
	public String partner;
	public String token;
	public String code; //protection code
	public int id;
	public List<WebAsset> me;
	public List<WebAsset> them;
	
	public String user1;
	public String user2;
	public List<String> list1;
	public List<String> list2;
	
	public class WebAsset {
		public String assetid;
		public String original_id;
		
		public String toString() {
			if (assetid == null) {
				return String.format("{\"original_id\":\"%s\"}", original_id);
			} else {
				return String.format("{\"assetid\":\"%s\", \"original_id\":\"%s\"}", assetid, original_id);
			}
		}
	}
}
