package trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.Gson;

import json.GetTradeOffers;
import json.GetTradeOffers.Offer;
import rtb.Browser;
import rtb.Log;
import rtb.Main;
import rtb.Main.Account;
import rtb.User;
import rtb.Log.LogType;
import rtb.Log.ThreadType;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class RejectThread extends Thread {
	
	private static final String DECLINE_TRADE_URL = "https://steamcommunity.com/tradeoffer/%s/decline";
	private static final String REFERER_URL = "https://steamcommunity.com/profiles/%s/tradeoffers";
	private static final String GET_RECEIVED_OFFERS = "https://api.steampowered.com/IEconService/GetTradeOffers/"
			+ "v1/?key=%s&get_received_offers=1&active_only=1";
	
	private Browser browser;
	private Main main;
	boolean isRunning;
	private Gson gson;
	
	public RejectThread(Main main, Browser browser) {
		this.browser = browser;
		this.main = main;
		
		gson = new Gson();
	}
	
	public boolean sendDecline(User botUser, String tradeOfferId) {
		String dataString = "";
		try {
			String sessionid = URLEncoder.encode(botUser.sessionID, "UTF-8");
			dataString = "sessionid=" + sessionid;
		} catch (UnsupportedEncodingException e) {
			Log.println(ThreadType.MAIN, LogType.INFO, "Failed to decline trade id: " + tradeOfferId +
					". UnsupportedEncodingException");
			return false;
		}
		String url = String.format(DECLINE_TRADE_URL, tradeOfferId);
		String referer = String.format(REFERER_URL, botUser.account.getId());
		
		HttpsURLConnection connection = browser.sendTrade(url, referer, dataString, botUser.cookieString);
		
		// Get status code
		int statusCode = 0;
		try {
            statusCode = connection.getResponseCode();
        } catch (IOException e) {
        	Log.println(ThreadType.TRADER, LogType.DEBUG,
        			"Unable to get status code when declining trade request: " + e.getStackTrace());
        }
		
		// trade cancellation accepted
		if(statusCode == 200) {
			Log.println(ThreadType.MAIN, LogType.INFO, "Trade id: " + tradeOfferId + " successfully declined");
			return true;
			
		} else {
			Log.println(ThreadType.MAIN, LogType.INFO, "Failed to decline trade id: " + tradeOfferId +
					". Status code: " + statusCode);
			return false;
		}
	}
	
	public void run() {
		isRunning = true;
		int startingInterval = 1000;
		int interval = startingInterval;
		
		while (isRunning) {
			// for each bot account
			for (Account account : Main.Account.values()) {
				HttpsURLConnection connection = browser.get(String.format(GET_RECEIVED_OFFERS, account.getKey()));
				
				// Get status code
				int statusCode = 0;
				try {
		            statusCode = connection.getResponseCode();
		        } catch (IOException e) {
		        	Log.println(ThreadType.TRADER, LogType.DEBUG,
		        			"Unable to get status code when requesting received offers: " + e.getStackTrace());
		        }
				
				String response = main.readHttpsResponse(connection);
			    if(statusCode == 200) {
			    	GetTradeOffers tradeOffers = gson.fromJson(response.toString(), GetTradeOffers.class);
			    	
			    	// if offers received is empty, go to next bot account
			    	if (tradeOffers.response.trade_offers_received == null) {
			    		continue;
			    	}
			    	
			    	// for each trade offer received, reject it
			    	User botUser = main.getUser(account.getUsername());
			    	for (Offer offer : tradeOffers.response.trade_offers_received) {
			    		if (offer.trade_offer_state == 2) {
			    			sendDecline(botUser, offer.tradeofferid);
			    		}
			    	}
			    }
			    
			}
			
			try {
				Thread.sleep(interval); //sleep for 1 second
			} catch (InterruptedException e) {
				Log.println(ThreadType.TRADER, LogType.DEBUG, "Sleep interrupted: " + e.getLocalizedMessage());
			}
		}
	}
}
