package trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.Gson;

import json.GetPlayerItems;
import json.TradeOffer;
import json.TradeOfferResponse;
import json.WebCommand;
import json.WebCommand.WebAsset;
import rtb.Browser;
import rtb.Item;
import rtb.LoadBalancer;
import rtb.Log;
import rtb.Main;
import rtb.PostgreSQLThread;
import rtb.Transaction;
import rtb.UpdateWebsiteThread;
import rtb.User;
import rtb.Log.LogType;
import rtb.Log.ThreadType;
import rtb.Main.Account;

/**
 * Sends a trade request based on the given parameters and adds the trade request to the
 * list of active trades
 * 
 * @author Ken Neth Yeoh
 *
 */
public class RequestThread extends Thread {

	public static final String TRADE_URL = "https://steamcommunity.com/tradeoffer/new/send";
	public static final String GET_PLAYER_ITEMS = "https://api.steampowered.com/IEconItems_570/GetPlayerItems/v0001/?key=%s&steamid=%s";
	
	private Main main;
	private LoadBalancer loadBalancer;
	private StatusThread tradeStatusThread;
	private PostgreSQLThread sqlthread;
	private Gson gson;
	private WebCommand command;
	private String botUsername;
	private int spaceNeeded;
	private Browser browser;
	
	public static enum Result {
		VALID, INVALID_DEPOSIT, INVALID_WITHDRAW, INVALID_LIST_1, INVALID_LIST_2
	}
	
	public RequestThread(Main main, LoadBalancer loadBalancer, Browser browser, StatusThread tradeStatusThread) {
		this.main = main;
		this.loadBalancer = loadBalancer;
		this.tradeStatusThread = tradeStatusThread;
		gson = new Gson();
		this.browser = browser;
	}
	
	public void init(PostgreSQLThread sqlthread, WebCommand command, String botUsername, int spaceNeeded) {
		this.sqlthread = sqlthread;
		this.command = command;
		this.botUsername = botUsername;
		this.spaceNeeded = spaceNeeded;
	}
	
	public boolean validMessage(WebCommand command) {
		// no msg field
		if (command == null) {
			Log.println(ThreadType.TRADER, LogType.INFO, "Empty msg received: " + command);
			return false;
		}
		
		if (command.msg == 0) {
			Log.println(ThreadType.TRADER, LogType.INFO, "Unknown msg received: " + command);
			return false;
		}
		
		// each message type expects different fields
		boolean valid = true;
		switch(command.msg) {
			case 1:
				if (command.steamid == null) { // no steamid field
					Log.println(ThreadType.TRADER, LogType.INFO, "steamid is missing");
					valid = false;
				}
				if (command.partner == null) { // no partner field
					Log.println(ThreadType.TRADER, LogType.INFO, "partner is missing");
					valid = false;
				}
				if (command.token == null) { // no token field
					Log.println(ThreadType.TRADER, LogType.INFO, "token is missing");
					valid = false;
				}
				if (command.code == null) { // no code field
					Log.println(ThreadType.TRADER, LogType.INFO, "code is missing");
					valid = false;
				}
				if ((Integer)command.id == null) { // no id field
					Log.println(ThreadType.TRADER, LogType.INFO, "id is missing");
					valid = false;
				}
				if (command.me == null) { // no me field
					Log.println(ThreadType.TRADER, LogType.INFO, "me is missing");
					valid = false;
				}
				if (command.them == null) { // no them field
					Log.println(ThreadType.TRADER, LogType.INFO, "them is missing");
					valid = false;
				}
				break;
			case 2:
				if ((Integer)command.id == null) { // no id field
					Log.println(ThreadType.TRADER, LogType.INFO, "id is missing");
					valid = false;
				}
				break;
			case 3:
				if ((Integer)command.id == null) { // no id field
					Log.println(ThreadType.TRADER, LogType.INFO, "id is missing");
					valid = false;
				}
				if (command.user1 == null) { // no user1 field
					Log.println(ThreadType.TRADER, LogType.INFO, "user1 is missing");
					valid = false;
				} else if (command.user1.equals("")) {
					Log.println(ThreadType.TRADER, LogType.INFO, "user1 is empty");
					valid = false;
				}
				if (command.user2 == null) { // no user2 field
					Log.println(ThreadType.TRADER, LogType.INFO, "user2 is missing");
					valid = false;
				} else if (command.user2.equals("")) {
					Log.println(ThreadType.TRADER, LogType.INFO, "user2 is empty");
					valid = false;
				}
				if (command.list1 == null) { // no list1 field
					Log.println(ThreadType.TRADER, LogType.INFO, "list1 is missing");
					valid = false;
				}
				if (command.list2 == null) { // no list2 field
					Log.println(ThreadType.TRADER, LogType.INFO, "list2 is missing");
					valid = false;
				}
				break;
			default:
				Log.println(ThreadType.TRADER, LogType.INFO, "Unknown msg received: " + command);
		}
		
		return valid;
	}
	
	public Result validTrade(WebCommand command) {
		// for each asset the bot is sending to the user
		for (WebAsset asset : command.me) {
			// check the user is the current owner of the item
			List<Item> usersItems = loadBalancer.getUsersItems(command.steamid);
			
			if (usersItems == null) {
				Log.println(ThreadType.TRADER, LogType.INFO, "Bots are not currently holding any items for user: " + command.steamid);
				return Result.INVALID_DEPOSIT;
			}
			
			boolean found = false;
			for (Item item : usersItems) {
				if (item.original_id.equals(asset.original_id)) {
					found = true;
				}
			}
			
			if (!found) {
				Log.println(ThreadType.TRADER, LogType.INFO, "User does not currently own item: " + asset.original_id);
				return Result.INVALID_DEPOSIT;
			}
		}
		
		// for each asset the user is sending to the bot
		//maybe skip this if it's slow?
		for (WebAsset asset : command.them) {
			// check we don't currently store this item
			if (loadBalancer.getItem(asset.assetid) != null) {
				Log.println(ThreadType.TRADER, LogType.INFO, "Bot is already storing item: " + asset.assetid);
				return Result.INVALID_WITHDRAW;
			}
		}
		
		return Result.VALID;
	}
	
	public String sendTradeOffer() throws TradeException {
		String referer = String.format("https://steamcommunity.com/tradeoffer/new/?partner=%s&token=%s",
										command.partner, command.token);
		
		TradeOffer offer;
		try {
			offer = buildOffer(command.me, command.them);
		} catch (TradeException e) {
			throw e;
		}
		
		User user = main.getUser(botUsername);
		
		String dataString = buildDataString(user.sessionID, command.steamid, command.code, gson.toJson(offer), command.token);
		
		HttpsURLConnection connection = browser.sendTrade(TRADE_URL, referer, dataString, user.cookieString);
		
		// Get status code
		int statusCode = 0;
		try {
            statusCode = connection.getResponseCode();
        } catch (IOException e) {
        	Log.println(ThreadType.TRADER, LogType.DEBUG, "Unable to get status code when sending trade request: " + e.getStackTrace());
        }
		
		String response = main.readHttpsResponse(connection);
		
		// trade offer went through
		if(statusCode == 200) {
			Log.println(ThreadType.MAIN, LogType.INFO, response);
			TradeOfferResponse tradeOfferResponse = gson.fromJson(response.toString(), TradeOfferResponse.class); //get trade offer ID
			
			// Add trade to DB as a transaction
			if (sqlthread.addTransaction(command.id, Arrays.toString(command.me.toArray()),
				Arrays.toString(command.them.toArray()), user.username, command.steamid,
				tradeOfferResponse.tradeofferid)) {
				Transaction transaction = new Transaction(
					Arrays.toString(command.me.toArray()), Arrays.toString(command.them.toArray()),
					1, user.username, Long.parseLong(command.steamid), tradeOfferResponse.tradeofferid);
				tradeStatusThread.addTransaction(command.id, transaction);
			}
			return tradeOfferResponse.tradeofferid;
		} else {
			Log.println(ThreadType.MAIN, LogType.DEBUG, "Status code: " + statusCode + " " + response);
			return "rejected";
		}
	}

	public TradeOffer buildOffer(List<WebAsset> me, List<WebAsset> them) throws TradeException {
		int version = me.size() + them.size() + 1;
		TradeOffer tradeOffer = new TradeOffer(true, version);
		
		// Add the items to 'send' in the trade offer
		GetPlayerItems playerItems = null;
		for (WebAsset webAsset : me) {
			// Convert original_id to assetid in items we are sending
			
			// if we need to get current inventory
			if (playerItems == null) {
				Account botAccount = main.getAccount(botUsername);
				HttpsURLConnection connection = browser.get(String.format(GET_PLAYER_ITEMS, botAccount.getKey(),
						botAccount.getId()));
				
				// Get status code
				int statusCode = 0;
				try {
		            statusCode = connection.getResponseCode();
		        } catch (IOException e) {
		        	Log.println(ThreadType.TRADER, LogType.DEBUG, "Unable to get status code when sending trade request: " + e.getStackTrace());
		        }
			    
			    String response = main.readHttpsResponse(connection);
			    
			    if(statusCode == 200) {
			    	playerItems = gson.fromJson(response.toString(), GetPlayerItems.class);
			    } else {
			    	throw new TradeException("Unable to get player items");
			    }
			}
			
			// search inventory for the assetid
			for (json.GetPlayerItems.Item item : playerItems.result.items) {
				// quick way to convert from int to String. webAsset.original_id is a String
				if ((item.original_id + "").equals(webAsset.original_id)) {
					tradeOffer.addAssetMe(570, "2", 1, item.id + "");
					break; //found the item, stop here
				}
			}
		}
		
		// Add the items to 'receive' in the trade offer
		for (WebAsset webAsset : them) {
			tradeOffer.addAssetThem(570, "2", 1, webAsset.assetid);
		}
		
		return tradeOffer;
	}
	
	public String buildDataString(String sessionId, String partner, String message, String offer, String token) {
		StringBuilder dataString = new StringBuilder();
//		dataString.append("sessionid=");
//		dataString.append(sessionId);
		
		dataString.append("&serverid=1");
		
		dataString.append("&partner=");
		dataString.append(partner);
		
		dataString.append("&tradeoffermessage=");
		
		if (!message.equals("")) {
			dataString.append("Trade offer code: " + message + ". Only trade if the trade offer code is "
					+ "identical to the code from Roshpit.ca");
		}
		
		dataString.append("&json_tradeoffer=");
		dataString.append(offer);
		
		dataString.append("&trade_offer_create_params={\"trade_offer_access_token\":\"");
		dataString.append(token);
		dataString.append("\"}");
		
		// encode data string for URL
		String result = "";
		try {
			//lazy way of escaping '=' and '&'
			result = URLEncoder.encode(dataString.toString(), "UTF-8").replace("%3D", "=").replace("%26", "&");
		} catch (UnsupportedEncodingException e) {
			Log.println(ThreadType.TRADER, LogType.DEBUG, "Error while encoding URL: " + e.getLocalizedMessage());
		}
		
		result = "sessionid=" + sessionId + result;
		
		return result;
	}
	
	public void run() {
		boolean spaceMade = loadBalancer.makeSpaceOnBot(botUsername, spaceNeeded);
		
		int numberOfTries = 0;
		int maxNumberOfTries = 5;
		int interval = 2000; //2 seconds
		
		String result = "rejected"; //will only stay this if steam doesn't respond
		
		while (numberOfTries < maxNumberOfTries) {
			if (spaceMade) {
				try {
					result = sendTradeOffer();
					break;
				} catch (TradeException e) {
					Log.println(ThreadType.TRADER, LogType.INFO, e.getLocalizedMessage());
					numberOfTries++;
				}
			} else {
				result = "load balancing failed";
				break;
			}
			
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				Log.println(ThreadType.TRADER, LogType.DEBUG, "Request sleep interrupted: " +
						e.getStackTrace());
			}
		}
		
    	UpdateWebsiteThread updateWebsiteThread = new UpdateWebsiteThread(browser, command.id, result);
    	updateWebsiteThread.start();
	}
}
