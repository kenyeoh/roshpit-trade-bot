package trade;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.HttpsURLConnection;

import json.GetPlayerItems;
import json.GetTradeOffers;
import json.GetTradeOffers.Offer;
import json.WebCommand.WebAsset;
import rtb.Browser;
import rtb.Item;
import rtb.LoadBalancer;
import rtb.Log;
import rtb.Main;
import rtb.PostgreSQLThread;
import rtb.Transaction;
import rtb.UpdateWebsiteThread;
import rtb.User;
import rtb.Log.LogType;
import rtb.Log.ThreadType;
import rtb.Main.Account;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * Periodically checks the status of all active trades if they were accepted/rejected/changed
 * Times out active trades if necessary
 * 
 * @author Ken Neth Yeoh
 *
 */
public class StatusThread extends Thread {
	
//	private static final String GET_SENT_OFFERS = "https://api.steampowered.com/IEconService/GetTradeOffers/"
//			+ "v1/?key=%s&get_sent_offers=1&active_only=1&time_historical_cutoff=%s";
	private static final int TRADE_TIME_OUT = 1800;
	private static final String GET_SENT_OFFERS = "https://api.steampowered.com/IEconService/GetTradeOffers/"
			+ "v1/?key=%s&get_sent_offers=1&active_only=1";
	

	private boolean isRunning;
	private ConcurrentHashMap<Integer, Transaction> activeTransactions; // k = id from roshpit
	private PostgreSQLThread sqlthread;
	private Gson gson;
	private Main main;
	private LoadBalancer loadBalancer;
	private Browser browser;
	
	public StatusThread(Main main, LoadBalancer loadBalancer, Browser browser) {
		this.main = main;
		this.loadBalancer = loadBalancer;
		this.browser = browser;
		gson = new Gson();
		activeTransactions = new ConcurrentHashMap<Integer, Transaction>();
	}
	
	public void setSQLThread(PostgreSQLThread sqlthread) {
		this.sqlthread = sqlthread;
	}
	
	public void setActiveTransactions(ConcurrentHashMap<Integer, Transaction> activeTransactions) {
		this.activeTransactions = activeTransactions;
	}
	
	public void addTransaction(int id, Transaction transaction) {
		activeTransactions.put(id, transaction);
	}
	
	public Transaction getTransaction(int id) {
		return activeTransactions.get(id);
	}
	
	public void transactionAccepted(Transaction tx) {
		Type listType = new TypeToken<List<WebAsset>>(){}.getType(); // for json parsing
		
		// add deposited items to database
		List<WebAsset> themAssets = gson.fromJson(tx.them, listType);
		
		if(themAssets.size() > 0) {
			if (sqlthread.addItems(tx.bot_username, tx.steamid, themAssets)) {
				for (WebAsset asset : themAssets) {
					Item item = new Item(tx.bot_username, tx.steamid, asset.original_id);
					loadBalancer.addItem(item);
					Log.println(ThreadType.TRADER, LogType.INFO, "Item " + asset.original_id + " successfully deposited");
				}
			}
		}
		
		// remove withdrawn items from database
		List<WebAsset> meAssets = gson.fromJson(tx.me, listType);
		
		if(meAssets.size() > 0) {
			if (sqlthread.removeItems(meAssets)) {
				for (WebAsset asset : meAssets) {
					loadBalancer.removeItem(tx.bot_username, tx.steamid, asset.original_id);
					Log.println(ThreadType.TRADER, LogType.INFO, "Item " + asset.original_id + " successfully withdrawn");
				}
			}
		}
	}
	
	public boolean transactionInvalidItems(Transaction tx) {
		// unknown/invalid status, check if items have been moved from inventory
		Account botAccount = main.getAccount(tx.bot_username);
		HttpsURLConnection connection = browser.get(String.format(RequestThread.GET_PLAYER_ITEMS, botAccount.getKey(),
				botAccount.getId()));
		
		// Get status code
		int statusCode = 0;
		try {
            statusCode = connection.getResponseCode();
        } catch (IOException e) {
        	Log.println(ThreadType.TRADER, LogType.DEBUG,
        			"Unable to get status code when getting backpack: " + e.getStackTrace());
        	return false;
        }
	    
	    String response = main.readHttpsResponse(connection);
	    
	    if(statusCode == 200) {
	    	// parse inventory into an object
	    	try {
	    		GetPlayerItems playerItems = gson.fromJson(response.toString(), GetPlayerItems.class);
	    		
	    		// check if items were transferred
		    	Type listType = new TypeToken<List<WebAsset>>(){}.getType(); // for json parsing
		    	
		    	// add deposited items to database
		    	List<WebAsset> themAssets = gson.fromJson(tx.them, listType);
		    	for (WebAsset asset : themAssets) {
		    		// search for this item in the bots inventory
		    		boolean found = false;
		    		for (json.GetPlayerItems.Item item : playerItems.result.items) {
		    			if ((item.original_id + "").equals(asset.original_id)) {
		    				// we found the item, update DB
		    				found = true;
		    				if (sqlthread.addItem(tx.bot_username, tx.steamid, asset.original_id)) {
				    			Log.println(ThreadType.TRADER, LogType.INFO,
				    					"Item " + asset.original_id + " successfully added");
				    			Item newItem = new Item(tx.bot_username, tx.steamid, asset.original_id);
				    			loadBalancer.addItem(newItem);
				    		}
		    			}
		    		}
		    		
		    		if (!found) {
		    			// trade did not go through
		    			Log.println(ThreadType.TRADER, LogType.INFO,
		    					"Item " + asset.original_id + " not found. Trade must have failed");
		    			return false;
		    		}
		    	}
		    	
		    	// remove withdrawn items from database
		    	List<WebAsset> meAssets = gson.fromJson(tx.me, listType);
		    	for (WebAsset asset : meAssets) {
		    		// search for this item in the bots inventory
		    		boolean found = false;
		    		for (json.GetPlayerItems.Item item : playerItems.result.items) {
		    			if ((item.original_id + "").equals(asset.original_id)) {
		    				found = true;
		    				break;
		    			}
		    		}
		    		
		    		if (found) {
		    			// we found the item, trade did not go through
		    			Log.println(ThreadType.TRADER, LogType.INFO,
		    					"Item " + asset.original_id + " still in bot inventory. Trade must have failed");
		    			return false;
		    		} else {
		    			// item not found, user must have received it
		    			if (sqlthread.removeItem(asset.original_id)) {
				    		Log.println(ThreadType.TRADER, LogType.INFO, "Item " + asset.original_id + " successfully withdrawn");
				    		loadBalancer.removeItem(tx.bot_username, tx.steamid, asset.original_id);
				    	}
		    		}
		    	}
	    	} catch (JsonSyntaxException e) {
	    		Log.println(ThreadType.TRADER, LogType.INFO, "Error JSON parsing backpack from steamid: " + botAccount.getId());
		    	return false;
	    	}
	    } else {
	    	Log.println(ThreadType.TRADER, LogType.INFO, "Unable to get player items");
	    	return false;
	    }
	    
	    return true;
	}
	
	public int getTransactionStatus(Transaction tx, List<Offer> offers) {
	    // iterate through trade offers and find the one with matching tradeofferid
	    for (Offer offer : offers) {
	    	if (offer.tradeofferid.equals(tx.tradeofferid)) {
	    		switch (offer.trade_offer_state) {
	    			case 2:
	    				// offer still valid, don't need to do anything
	    				return 2;
	    			case 3:
	    				Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    						"Steamid: %d accepted trade request: %s", tx.steamid,
	    						offer.tradeofferid));
	    				transactionAccepted(tx);
				    	return 3;
	    			case 4:
	    				Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    						"Steamid: %d made a counter offer on trade: %s, will be rejected",
	    						tx.steamid, offer.tradeofferid));
	    				// counter offers count as new received trade offers, so
	    				// we don't need to do anything here
	    				return 4;
	    			case 5:
	    				Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    						"Trade: %s expired", offer.tradeofferid));
	    				return 5;
	    			case 6:
	    				Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    						"We cancelled trade: %s", offer.tradeofferid));
	    				return 6;
	    			case 7:
	    				Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    						"Steamid: %d declined trade request: %s", tx.steamid,
	    						offer.tradeofferid));
	    				return 7;
	    			case 8:
	    				Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    						"Trade: %s in invalid state, checking inventory",
	    						offer.tradeofferid));
	    				boolean tradeResult = transactionInvalidItems(tx);
	    				if (tradeResult) {
	    					return 3; //trade must have gone through, even through steam API says otherwise
	    				} else {
	    					return 8;
	    				}
	    			default: //should not be reached
	    				Log.println(ThreadType.TRADER, LogType.INFO, "Unknown state");
	    				return -1;
	    		}
	    	}
	    }
	    
	    Log.println(ThreadType.TRADER, LogType.INFO, String.format(
	    		"Active transaction %s not found", tx.tradeofferid));
	    return -1;
	}
	
	public int getTradeTime(Transaction tx, List<Offer> offers) {
		// iterate through trade offers and find the one with matching tradeofferid
	    for (Offer offer : offers) {
	    	if (offer.tradeofferid.equals(tx.tradeofferid)) {
	    		return offer.time_created;
	    	}
	    }
	    return 0;
	}
	
	public void run() {
		isRunning = true;
		HashMap<String, GetTradeOffers> botTradeOffers = new HashMap<String, GetTradeOffers>();
		while (isRunning) {
			botTradeOffers.clear();
			
			Iterator<Entry<Integer, Transaction>> it = activeTransactions.entrySet().iterator();
			
			while (it.hasNext()) {
		        Map.Entry<Integer, Transaction> pairs = (Entry<Integer, Transaction>)it.next();
		        Integer id = pairs.getKey();
			    Transaction tx = pairs.getValue();
		        
			    // check if we have already obtained a trade offer for this bot account
			    GetTradeOffers tradeOffers = botTradeOffers.get(tx.bot_username);
			    
			    // we need to get trade offers for this bot account
			    if (tradeOffers == null) {
			    	Account botAccount = main.getAccount(tx.bot_username);
			    	HttpsURLConnection connection = browser.get(String.format(GET_SENT_OFFERS,
			    			botAccount.getKey()));
//			    	HttpsURLConnection connection = browser.get(String.format(GET_SENT_OFFERS, botAccount.getKey(),
//			    			System.currentTimeMillis() / 1000L - TRADE_TIME_OUT));
			    	
			    	// Get status code
					int statusCode = 0;
					try {
			            statusCode = connection.getResponseCode();
			        } catch (IOException e) {
			        	Log.println(ThreadType.TRADER, LogType.DEBUG,
			        			"Unable to get status code when sending trade request: " + e.getStackTrace());
			        }
				    
				    String response = main.readHttpsResponse(connection);
				    if(statusCode == 200) {
				    	tradeOffers = gson.fromJson(response.toString(), GetTradeOffers.class);
				    	botTradeOffers.put(tx.bot_username, tradeOffers);
				    } else {
				    	Log.println(ThreadType.TRADER, LogType.INFO, "Unable to get active trade offers");
				    	continue; //finish here since getting trade offers failed
				    }
			    }
			    
			    int tradeStatus = getTransactionStatus(tx, tradeOffers.response.trade_offers_sent);
		    	// remove this transaction from active transactions if no longer active
			    // 2 = active, 4 = counter offer
			    if (tradeStatus != 2) {
			    	// update DB
			    	if (sqlthread.updateTransaction(id, tradeStatus)) {
    					Log.println(ThreadType.TRADER, LogType.INFO,
							"Updated transaction " + tx.tradeofferid + " to: " + tradeStatus);
    				}
			    	
			    	it.remove();
			    	
			    	//tell website the status update
			    	int websiteStatus = 0;
			    	switch (tradeStatus) {
				    	case 3: //accepted
				    		websiteStatus = 1;
				    		break;
				    	case 4: //user made counter offer
				    		websiteStatus = 3;
				    		break;
				    	case 6: //timeout, and we cancelled
				    		websiteStatus = 4;
				    		break;
				    	case 7: //rejected by user
				    		websiteStatus = 2;
				    		break;
			    		default: // unknown
			    			Log.println(ThreadType.TRADER, LogType.DEBUG,
									"Unexpected trade status: " + tradeStatus);
			    			break;
			    	}
			    	
			    	//kick off a new thread to update the website
			    	UpdateWebsiteThread updateWebsiteThread = new UpdateWebsiteThread(browser, id, websiteStatus);
			    	updateWebsiteThread.start();
			    } else {
			    	//TODO decline timed out trades
			    	//trade status is active, check if we need to time it out
			    	int tradeTime = getTradeTime(tx, tradeOffers.response.trade_offers_sent);
			    	int unixTime = (int) (System.currentTimeMillis() / 1000L);
			    	
			    	// trade has expired, cancel it
			    	if (unixTime - tradeTime > TRADE_TIME_OUT) {
			    		User botUser = main.getUser(tx.bot_username);
			    		
			    		//kick off a new thread to cancel the trade
			    		CancelThread cancelThread = new CancelThread(browser, botUser, tx.tradeofferid);
			    		cancelThread.start();
			    	}
			    }
		    }
			
			try {
				Thread.sleep(1000); //sleep for 1 second
			} catch (InterruptedException e) {
				Log.println(ThreadType.TRADER, LogType.DEBUG, "Sleep interrupted: " + e.getLocalizedMessage());
			}
		}
	}
}
