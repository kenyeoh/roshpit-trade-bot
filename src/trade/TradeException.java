package trade;

public class TradeException extends Exception {
	private static final long serialVersionUID = -7029084232850340028L;

	public TradeException(String message) {
        super(message);
    }
}