package trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import rtb.Browser;
import rtb.Log;
import rtb.User;
import rtb.Log.LogType;
import rtb.Log.ThreadType;

/**
 * 
 * @author Ken Neth Yeoh
 *
 */
public class CancelThread extends Thread {
	
	private static final String CANCEL_TRADE_URL = "https://steamcommunity.com/tradeoffer/%s/cancel";
	private static final String REFERER_URL = "https://steamcommunity.com/profiles/%s/tradeoffers/sent/";
	
	private Browser browser;
	private User botUser;
	private String tradeOfferId;
	
	public CancelThread(Browser browser, User botUser, String tradeOfferId) {
		this.browser = browser;
		this.botUser = botUser;
		this.tradeOfferId = tradeOfferId;
	}
	
	public boolean sendCancel() {
		String dataString = "";
		try {
			String sessionid = URLEncoder.encode(botUser.sessionID, "UTF-8");
			dataString = "sessionid=" + sessionid;
		} catch (UnsupportedEncodingException e) {
			Log.println(ThreadType.MAIN, LogType.INFO, "Failed to cancel trade id: " + tradeOfferId +
					". UnsupportedEncodingException");
			return false;
		}
		String url = String.format(CANCEL_TRADE_URL, tradeOfferId);
		String referer = String.format(REFERER_URL, botUser.account.getId());
		
		HttpsURLConnection connection = browser.sendTrade(url, referer, dataString, botUser.cookieString);
		
		// Get status code
		int statusCode = 0;
		try {
            statusCode = connection.getResponseCode();
        } catch (IOException e) {
        	Log.println(ThreadType.TRADER, LogType.DEBUG,
        			"Unable to get status code when cancelling trade request: " + e.getStackTrace());
        }
		
		// trade cancellation accepted
		if(statusCode == 200) {
			Log.println(ThreadType.MAIN, LogType.INFO, "Trade id: " + tradeOfferId + " successfully cancelled");
			return true;
			
		} else {
			Log.println(ThreadType.MAIN, LogType.INFO, "Failed to cancel trade id: " + tradeOfferId +
					". Status code: " + statusCode);
			return false;
		}
	}
	
	public void run() {
		sendCancel();
	}
}
