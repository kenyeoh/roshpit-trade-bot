package trade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import rtb.Browser;
import rtb.Item;
import rtb.LoadBalancer;
import rtb.Log;
import rtb.Main;
import rtb.PostgreSQLThread;
import rtb.UpdateWebsiteThread;
import rtb.User;
import rtb.Log.LogType;
import rtb.Log.ThreadType;
import rtb.Main.Account;
import trade.RequestThread.Result;
import json.GetPlayerItems;
import json.GetTradeOffers;
import json.TradeOffer;
import json.TradeOfferResponse;
import json.WebCommand;
import json.GetTradeOffers.Offer;

import com.google.gson.Gson;

public class SwapThread extends Thread {
	private Main main;
	private LoadBalancer loadBalancer;
	private Gson gson;
	private PostgreSQLThread sqlthread;
	private WebCommand command;
	private Browser browser;
	private StatusThread tradeStatusThread;
	
	private static final String GET_INCOMING_TRADES = "https://api.steampowered.com/IEconService/GetTradeOffers/"
			+ "v1/?key=%s&get_received_offers=1&active_only=1";
	private static final String ROSHPIT_OFFER_RESULTS = "https://roshpit.ca/market/receive_user_trade";

	public SwapThread(Main main, LoadBalancer loadBalancer, Browser browser, StatusThread tradeStatusThread) {
		this.main = main;
		this.loadBalancer = loadBalancer;
		this.browser = browser;
		this.tradeStatusThread = tradeStatusThread;
		gson = new Gson();
	}
	
	public void init(PostgreSQLThread sqlthread, WebCommand command) {
		this.sqlthread = sqlthread;
		this.command = command;
	}
	
	public Result validItemExchange() {
		// for each item (assetid) user1 is sending to user2
		for (String original_id : command.list1) {
			// check user1 is the current owner of the item
			List<Item> usersItems = loadBalancer.getUsersItems(command.user1);
			
			if (usersItems != null) {
				boolean found = false;
				for (Item item : usersItems) {
					if (item.original_id.equals(original_id)) {
						found = true;
					}
				}
				
				if (!found) {
					Log.println(ThreadType.TRADER, LogType.INFO, "User1 does not currently own item: " + original_id);
					return Result.INVALID_LIST_1;
				}
			}
		}
		
		// for each item (assetid) user2 is sending to user1
		for (String original_id : command.list2) {
			// check user2 is the current owner of the item
			List<Item> usersItems = loadBalancer.getUsersItems(command.user2);
			
			if (usersItems != null) {
				boolean found = false;
				for (Item item : usersItems) {
					if (item.original_id.equals(original_id)) {
						found = true;
					}
				}
				
				if (!found) {
					Log.println(ThreadType.TRADER, LogType.INFO, "User2 does not currently own item: " + original_id);
					return Result.INVALID_LIST_2;
				}
			}
		}
		
		return Result.VALID;
	}
	
	public void itemSwap(ArrayList<Item> sender, ArrayList<Item> receiver,
			 List<String> itemsToSend, long receiverSteamid, String newBot) {
		// for each item the sender wants to send
		for (String original_id : itemsToSend) {
			// for each item the sender has
			for (Iterator<Item> iterator = sender.iterator(); iterator.hasNext();) {
				Item item = iterator.next();
				
				// if the item matches
				if (original_id.equals(item.original_id)) {
					// update DB
					if (sqlthread.updateItemOwnerAndBot(item.original_id, newBot, receiverSteamid + "")) {
						// add item to receiver
						receiver.add(item);
						
						//update item owner
						item.steamid = receiverSteamid;
						
						// remove item from sender
						iterator.remove();
					}
				}
			}
		}
	}
	
	public void itemSwap(ArrayList<Item> sender, ArrayList<Item> receiver,
						 List<String> itemsToSend, long receiverSteamid) {
		// for each item the sender wants to send
		for (String original_id : itemsToSend) {
			// for each item the sender has
			for (Iterator<Item> iterator = sender.iterator(); iterator.hasNext();) {
				Item item = iterator.next();
				
				// if the item matches
				if (original_id.equals(item.original_id)) {
					// update DB
					if (sqlthread.updateItemOwner(item.original_id, receiverSteamid + "")) {
						// add item to receiver
						receiver.add(item);
						
						//update item owner
						item.steamid = receiverSteamid;
						
						// remove item from sender
						iterator.remove();
					}
				}
			}
		}
	}
	
	public boolean acceptTradeOffer(Account account2, TradeOfferResponse tradeOfferResponse) {
		HttpsURLConnection connection = browser.get(String.format(GET_INCOMING_TRADES, account2.getKey()));
		
		String response = main.readHttpsResponse(connection);
		
		// Get status code
		int statusCode = 0;
		try {
            statusCode = connection.getResponseCode();
        } catch (IOException e) {
        	Log.println(ThreadType.TRADER, LogType.DEBUG, "Unable to get status code when getting trade offers: " + e.getStackTrace());
        }
		
	    if(statusCode == 200) {
	    	GetTradeOffers tradeOffers = gson.fromJson(response.toString(), GetTradeOffers.class);
	    	for (Offer offer : tradeOffers.response.trade_offers_received) {
	    		if (offer.tradeofferid.equals(tradeOfferResponse.tradeofferid)) {
	    			if (offer.trade_offer_state == 2) { //active
	    				//accept the trade
	    				User user2 = main.getUser(account2.getUsername()); //for getting cookies
	    				
	    				String formData = String.format("sessionid=%s&serverid=%s&tradeofferid=%s&partner=%s",
	    						user2.sessionID, "1", offer.tradeofferid, offer.accountid_other);
	    				
	    				HttpsURLConnection tradeConnection = browser.sendTradeAccept(offer.tradeofferid, formData, user2);
	    				
	    				int tradeStatusCode = 0;
	    				try {
	    					tradeStatusCode = tradeConnection.getResponseCode();
	    				} catch (IOException e) {
	    					Log.println(ThreadType.TRADER, LogType.DEBUG,
	    							"Unable to get status code when accepting a trade offer: " + e.getStackTrace());
	    				}
	    				
	    				if (tradeStatusCode == 200) {
	    					return true;
	    				}
	    			}
	    		}
	    	}
	    	
	    } else {
	    	Log.println(ThreadType.TRADER, LogType.INFO, "Unable to get active trade offers");
	    	return false; //finish here since getting trade offers failed
	    }
	    
		return false;
	}
	
	//1 = accepted, 2 = rejected by steam, 3 = steam is down (timed out)
	public int itemExchange() {
		//get the bots which contain items for each user
		ArrayList<Item> user1Items = loadBalancer.getUsersItems(command.user1);
		ArrayList<Item> user2Items = loadBalancer.getUsersItems(command.user2);
		
		// user 1 is new
		if(user1Items == null) {
			// items can only be transferred from user2 to user1 since user1 has no items
			ArrayList<Item> user1NewItems = new ArrayList<Item>();
			long steamidUser1 = Long.parseLong(command.user1);
			
			itemSwap(user2Items, user1NewItems, command.list2, steamidUser1);
			
			loadBalancer.addUserItems(steamidUser1, user1NewItems);
			
			Log.println(ThreadType.TRADER, LogType.INFO, "User1 is new, updating DB only");
			return 1;
		} else if(user2Items == null) {
			// user 2 is new
			// items can only be transferred from user1 to user2 since user2 has no items
			ArrayList<Item> user2NewItems = new ArrayList<Item>();
			long steamidUser2 = Long.parseLong(command.user2);
			
			itemSwap(user1Items, user2NewItems, command.list1, steamidUser2);
			
			loadBalancer.addUserItems(steamidUser2, user2NewItems);
			
			Log.println(ThreadType.TRADER, LogType.INFO, "User2 is new, updating DB only");
			return 1;
		} else {
			// neither user is new, check which bot is storing their items
			String bot1 = user1Items.get(0).botUsername;
			String bot2 = user2Items.get(0).botUsername;
			
			long steamidUser1 = Long.parseLong(command.user1);
			long steamidUser2 = Long.parseLong(command.user2);
			
			// items are currently stored on the same bot
			if(bot1.equals(bot2)) {
				// give items from user1 to user2
				itemSwap(user1Items, user2Items, command.list1, steamidUser2);
				
				// give items from user2 to user1
				itemSwap(user2Items, user1Items, command.list2, steamidUser1);
				
				Log.println(ThreadType.TRADER, LogType.INFO, "Items stored on same bot, updating DB only");
				return 1;
			} else {
				//need to trade items between bots here
				boolean accepted = false;
				try {
					TradeOffer offer = buildOffer();
					
					User user1 = main.getUser(bot1);
					Account account2 = main.getAccount(bot2);
					
					RequestThread requestThread = new RequestThread(main, loadBalancer, browser, tradeStatusThread);
					
					String dataString = requestThread.buildDataString(user1.sessionID,
							account2.getId(), "", gson.toJson(offer), account2.getToken());
					
					String referer = String.format("https://steamcommunity.com/tradeoffer/new/?partner=%s&token=%s",
													account2.getPartner(), account2.getToken());
					HttpsURLConnection sendConnection = browser.sendTrade(RequestThread.TRADE_URL,
							referer, dataString, user1.cookieString);
					
					// Get status code
					int statusCode = 0;
					try {
			            statusCode = sendConnection.getResponseCode();
			        } catch (IOException e) {
			        	Log.println(ThreadType.TRADER, LogType.DEBUG,
			        			"Unable to get status code when sending trade request: " + e.getStackTrace());
			        }
					
					// Print output based on status code
					String response = main.readHttpsResponse(sendConnection);
					if(statusCode == 200) {
						// trade offer went through
						// get trade offer ID
						TradeOfferResponse tradeOfferResponse = gson.fromJson(response.toString(), TradeOfferResponse.class);
						
						accepted = acceptTradeOffer(account2, tradeOfferResponse);
					} else {
						Log.println(ThreadType.TRADER, LogType.DEBUG, "Unknown status code: " + statusCode);
					}
				} catch (TradeException e) {
					Log.println(ThreadType.TRADER, LogType.INFO, e.getLocalizedMessage());
					return 3;
				}
				
			    if (accepted) {
			    	Log.println(ThreadType.TRADER, LogType.INFO, "Swapped items between " + bot1 + " and " + bot2);
			    	
			    	// give items from user1 to user2
					itemSwap(user1Items, user2Items, command.list1, steamidUser2, bot2);
					
					// give items from user2 to user1
					itemSwap(user2Items, user1Items, command.list2, steamidUser1, bot1);
					return 1;
			    } else {
			    	return 3;
			    }
			}
		}
	}
	
	private TradeOffer buildOffer() throws TradeException {
		int version = command.list1.size() + command.list2.size() + 1;
		TradeOffer tradeOffer = new TradeOffer(true, version);
		
		// get the bot username of the bot holding user1's items
		String bot1 = loadBalancer.getDepositBot(command.user1);
		
		// Add the items to 'send' in the trade offer
		GetPlayerItems player1Items = null;
		for (String original_id : command.list1) {
			// Convert original_id to assetid in items we are sending
			
			// if we need to get current inventory
			if (player1Items == null) {
				Account botAccount = main.getAccount(bot1);
				HttpsURLConnection connection = browser.get(String.format(
					RequestThread.GET_PLAYER_ITEMS, botAccount.getKey(), botAccount.getId()));
				
				// Get status code
				int statusCode = 0;
				try {
		            statusCode = connection.getResponseCode();
		        } catch (IOException e) {
		        	Log.println(ThreadType.TRADER, LogType.DEBUG, "Unable to get status code when sending trade request: " + e.getStackTrace());
		        }
			    
			    String response = main.readHttpsResponse(connection);
			    
			    if(statusCode == 200) {
			    	player1Items = gson.fromJson(response.toString(), GetPlayerItems.class);
			    } else {
			    	throw new TradeException("Unable to get player1 items");
			    }
			}
			
			// search inventory for the assetid
			for (json.GetPlayerItems.Item item : player1Items.result.items) {
				// quick way to convert from int to String. webAsset.original_id is a String
				if ((item.original_id + "").equals(original_id)) {
					tradeOffer.addAssetMe(570, "2", 1, item.id + "");
					break; //found the item, stop here
				}
			}
		}
		
		// get the bot username of the bot holding user2's items
		String bot2 = loadBalancer.getDepositBot(command.user2);
		
		// Add the items to 'send' in the trade offer
		GetPlayerItems player2Items = null;
		for (String original_id : command.list2) {
			// Convert original_id to assetid in items we are sending
			
			// if we need to get current inventory
			if (player2Items == null) {
				Account botAccount = main.getAccount(bot2);
				HttpsURLConnection connection = browser.get(String.format(
					RequestThread.GET_PLAYER_ITEMS, botAccount.getKey(), botAccount.getId()));
				
				// Get status code
				int statusCode = 0;
				try {
		            statusCode = connection.getResponseCode();
		        } catch (IOException e) {
		        	Log.println(ThreadType.TRADER, LogType.DEBUG, "Unable to get status code when sending trade request: " + e.getStackTrace());
		        }
			    
			    String response = main.readHttpsResponse(connection);
			    
			    if(statusCode == 200) {
			    	player2Items = gson.fromJson(response.toString(), GetPlayerItems.class);
			    } else {
			    	throw new TradeException("Unable to get player2 items");
			    }
			}
			
			// search inventory for the assetid
			for (json.GetPlayerItems.Item item : player2Items.result.items) {
				// quick way to convert from int to String. webAsset.original_id is a String
				if ((item.original_id + "").equals(original_id)) {
					tradeOffer.addAssetThem(570, "2", 1, item.id + "");
					break; //found the item, stop here
				}
			}
		}
		
		return tradeOffer;
	}

	public void run() {
		int numberOfTries = 0;
		int maxNumTries = 5;
		int interval = 2000; //2 seconds
		
		//TODO fix this
		// get the bots for bot users
		String user1Bot = loadBalancer.getDepositBot(command.user1);
		String user2Bot = loadBalancer.getDepositBot(command.user2);
		
		// check which bot will end up with more items
		int deltaItems = command.list1.size() - command.list2.size();
		
		// if user1 gives more items to user2, make space on user2
		boolean spaceMade = false;
		if (deltaItems > 0) {
			spaceMade = loadBalancer.makeSpaceOnBot(user2Bot, deltaItems);
		} else if (deltaItems < 0) {
			// user2 gives more items than user1
			spaceMade = loadBalancer.makeSpaceOnBot(user1Bot, deltaItems*-1); //convert to positive
		}
		
		int itemsExchanged = 0;
		
		while (numberOfTries < maxNumTries) {
			itemsExchanged = itemExchange();
			
			if (itemsExchanged == 1 || itemsExchanged == 2) {
				break;
			}
			
			numberOfTries++;
			
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				Log.println(ThreadType.TRADER, LogType.DEBUG, "Swap sleep interrupted: " +
						e.getStackTrace());
			}
		}
		
		//kick off a new thread to update the website
    	UpdateWebsiteThread updateWebsiteThread = new UpdateWebsiteThread(browser, command.id,
    			itemsExchanged, 1);
    	updateWebsiteThread.start();
	}
}
